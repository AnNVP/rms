(function () {
    'use strict';

    angular.module('BlurAdmin.shared', [
        'BlurAdmin.shared.directives',
        'BlurAdmin.shared.services'
    ]);

})();
