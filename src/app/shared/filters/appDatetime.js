(function () {
    'use strict';

    angular.module('BlurAdmin.shared')
        .filter('appDatetime', appDatetime);

    /** @ngInject */
    function appDatetime($filter, $cookies) {
        return function (input) {
            if (!input) {
                return '';
            }
            return $filter('date')(moment.utc(input).local().toDate(),
                ($cookies.get('NG_TRANSLATE_LANG_KEY') === 'en' ? 'MM/dd/yyyy HH:mm' : 'dd/MM/yyyy HH:mm'));
        };
    }

})();
