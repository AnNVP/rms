(function () {
    'use strict';

    angular.module('BlurAdmin.shared')
        .filter('mergingTypeColor', mergingTypeColor);

    /** @ngInject */
    function mergingTypeColor($filter) {
        return function (input) {
            var type = $filter('mergingType')(input)
            if (type >= 24 && type <= 31) {
                return 'merging-type-4';
            } else if (type >= 16 && type <= 23) {
                return 'merging-type-3';
            } else if (type >= 8 && type <= 15) {
                return 'merging-type-2';
            } else if (type >= 0 && type <= 7) {
                return 'merging-type-1';
            } else {
                return null;
            }
        };
    }

})();
