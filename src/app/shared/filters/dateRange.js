(function () {
  'use strict';

  angular.module('BlurAdmin.shared')
    .filter('dateRange', dateRange);

  /** @ngInject */
  function dateRange(datetimeService) {
    return function (startDate, endDate, isFormat, format) {
      return datetimeService.dateRange(startDate, endDate, isFormat, format);
    };
  }

})();
