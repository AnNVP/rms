(function () {
    'use strict';

    angular.module('BlurAdmin.shared')
        .filter('mergingType', mergingType);

    /** @ngInject */
    function mergingType($filter) {
        return function (input) {
            if (!input) {
                return null;
            }
            return _.parseInt(input, 2);
        };
    }

})();
