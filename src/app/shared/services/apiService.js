(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('apiService', apiService);

  /** @ngInject */
  function apiService($http, $window, $cookies, $timeout,
    config, notificationService, loaderService) {

    var onErrorRequest = function (res) {
      if (res.status === 401) {
        _.forEach($cookies.getAll(), function (value, key) {
          $cookies.remove(key);
        });
        $window.location.href = 'auth.html';
      } else if (res.status === -1) {
        notificationService.errorTranslate('ERR_CONNECTION_REFUSED');
      } else {
        if (_.has(res, 'data.message')) {
          notificationService.errorTranslate(res.data.message);
        } else {
          notificationService.errorTranslate('ERROR_OCCURRED');
        }
      }
    };

    var request = function (url, method, data, successCallback, errorCallback, options, fileDownloadName) {
      loaderService.show();
      var isGet = (method.toLowerCase() === 'get');
      options = _.merge(options, {
        url: config.apiUrl + url,
        method: method,
        params: (isGet ? data : null),
        data: (!isGet ? (data || null) : null)
      });

      $http(options)
        .then(function success(response) {
          if (!isGet) {
            notificationService.successTranslate('SUCCESS');
          }
          // show loading a moment
          $timeout(function () {
            loaderService.hide();
            // check if is download
            if (fileDownloadName) {
              var blob = new Blob([response.data], { type: response.headers("content-type") });
              saveAs(blob, fileDownloadName);
            }
            if (successCallback) {
              successCallback(response.data, response.status);
            }
          }, 400);
        }, function error(errorResponse) {
          loaderService.hide();
          onErrorRequest(errorResponse);
          if (errorCallback) {
            errorCallback(errorResponse, errorResponse.status);
          }
        }
        );
    };

    function get(url, data, successCallback, errorCallback, options) {
      request(url, 'get', data, successCallback, errorCallback, options);
    };

    function create(url, data, successCallback, errorCallback, options) {
      request(url, 'post', data, successCallback, errorCallback, options);
    };

    function update(url, data, successCallback, errorCallback, options) {
      request(url, 'put', data, successCallback, errorCallback, options);
    };

    function remove(url, successCallback, errorCallback, data, options) {
      request(url, 'delete', data, successCallback, errorCallback, options);
    };

    function patch(url, data, successCallback, errorCallback, options) {
      request(url, 'patch', data, successCallback, errorCallback, options);
    };

    function download(method, url, data, fileDownloadName, successCallback, errorCallback, options) {
      options = _.merge(options, {
        responseType: 'arraybuffer'
      });
      request(url, method, data, successCallback, errorCallback, options, fileDownloadName);
    };

    return {
      request: request,
      get: get,
      create: create,
      update: update,
      delete: remove,
      patch: patch,
      download: download,
    };

  }
})();