(function () {
  'use strict';

  angular.module('BlurAdmin.shared.services')
    .factory('datetimeService', datetimeService);

  /** @ngInject */
  function datetimeService($cookies) {

    function dateRange(startDate, endDate, isFormat, format) {
      if (!format) {
        format = ($cookies.get('NG_TRANSLATE_LANG_KEY') === 'en' ? 'MM/DD/YYYY' : 'DD/MM/YYYY');
      }

      startDate = moment(startDate);
      endDate = moment(endDate);

      if (startDate.isSame(endDate)) {
        return [!isFormat ? startDate.toDate() : startDate.format(format)];
      }

      var range = [];
      for (var date = startDate; date.isBefore(endDate); date.add(1, 'days')) {
        range.push(!isFormat ? date.toDate() : date.format(format));
      }
      return range;
    }

    return {
      dateRange: dateRange,
    };
  }
})();