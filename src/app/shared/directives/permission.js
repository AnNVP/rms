(function () {
    'use strict';

    angular.module('BlurAdmin.shared.directives')
        .directive('permission', permission);

    /** @ngInject */
    function permission() {
        return {
            restrict: 'A',
            scope: {
                route: '@permission',
            },
            controller: function ($scope, $element, permissionService) {
                if (permissionService.isDenied($scope.route)) {
                    $element.remove();
                }
            }
        };
    }

})();