(function () {
  'use strict';

  angular.module('BlurAdmin.shared.directives')
    .directive('searchDate', searchDate);

  /** @ngInject */
  function searchDate() {
    return {
      restrict: 'E',
      scope: {
        type: '@',
        label: '@',
        default: '=',
        column: '@',
        tableState: '=',
        refresh: '&',
      },
      templateUrl: 'app/shared/directives/search/date/searchDate.html',
      link: function (scope) {
        scope.datePicker = {
          date: { startDate: scope.default, endDate: scope.default },
          options: {
            autoApply: true,
            singleDatePicker: true,
            showDropdowns: true,
            eventHandlers: {
              'apply.daterangepicker': function () {
                scope.onChange();
              }
            }
          },
        };

        scope.onChange = function () {
          if (_.isUndefined(scope.tableState.all)) {
            scope.tableState.all = {};
          }
          if (_.isUndefined(scope.tableState.all['date'])) {
            scope.tableState.all['date'] = [];
          }
          var indexed = _.findIndex(scope.tableState.all['date'], function (o) {
            return o.type === scope.type && _.keys(o.data)[0] === scope.column;
          });
          var data = {};
          data[scope.column] = scope.datePicker.date.toDate();
          var search = { type: scope.type, data: data };
          if (indexed === -1) {
            scope.tableState.all['date'].push(search);
          } else {
            scope.tableState.all['date'].splice(indexed, 1, search);
          }
          scope.tableState.pagination.start = 0;
          scope.refresh();
        };
      }
    };
  }

})();