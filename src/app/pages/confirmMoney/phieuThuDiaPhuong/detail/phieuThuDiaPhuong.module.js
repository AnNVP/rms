(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.confirmMoney')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('confirmMoney.phieuThuDiaPhuongDetail', {
          url: '/confirm-money-incoming-TMSN/{phieuThuDiaPhuongId}',
          title: 'PHIEU_THU_DIA_PHUONG',
          templateUrl: 'app/pages/confirmMoney/phieuThuDiaPhuong/detail/phieu-thu-dia-phuong.html',
          controller: 'ConfirmMoneyPhieuThuDiaPhuongPageCtrl',
        });
    }
  
  })();
  