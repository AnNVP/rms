(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('confirmMoney.phieuThuDiaPhuongs', {
        url: '/confirm-money-incoming-TMSN',
        title: 'PHIEU_THU_DIA_PHUONG',
        templateUrl: 'app/pages/confirmMoney/phieuThuDiaPhuong/phieu-thu-dia-phuongs.html',
        controller: 'ConfirmMoneyPhieuThuDiaPhuongsPageCtrl',
        sidebarMeta: {
          order: 0,
        },
        'permission': 'Smss.GetUnextractedSMSTypePhieuThuDiaPhuong'
      });
  }

})();
