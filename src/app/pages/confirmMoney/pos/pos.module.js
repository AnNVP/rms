(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.confirmMoney')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('confirmMoney.pos', {
          url: '/pos',
          title: 'POS',
          templateUrl: 'app/pages/confirmMoney/pos/pos.html',
          controller: 'PosPageCtrl',
          sidebarMeta: {
            order: 0,
          },
          'permission': 'Smss.GetListSmsTypePos'
        });
    }
  
  })();
  