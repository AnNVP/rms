(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .controller('PosPageCtrl', PosPageCtrl);

  /** @ngInject */
  function PosPageCtrl($scope, apiService, confirmService) {

    $scope.pos = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: '',
        }
      },
      transStatus: [
        { value: '', label: 'Tất cả' },
        { value: 'S1', label: 'S1' },
        { value: 'S2', label: 'S2' },
        { value: 'S3', label: 'S3' },
        { value: 'S4', label: 'S4' },
        { value: 'S8', label: 'S8' }
      ],
      bankLabel: {
        'Techcombank': 'TECH',
        'VietinBank': 'VIETIN',
        'BIDV': 'BIDV',
        'Vietcombank': 'VIETCOM',
        'ACB': 'ACB',
        'Sacombank': 'SACOM',
        'AGRIBANK': 'AGR',
        'VPBank': 'VP'
      },
      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],
      init: function (tableState) {
        $scope.pos.tableState = tableState;
        $scope.pos.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.loading = true;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{ orderCode: _this.default.search.orderCode }];
          searchInit.all['date'] = [
            { type: 'greater', data: { sentTime: _this.default.search.startDate.toDate() } },
            { type: 'lesser', data: { sentTime: _this.default.search.endDate.toDate() } }
          ];
        }
        _this.isInit = true;
        apiService.get('smss/unextracted-sms-type-pos', searchInit, function (response) {
          _this.tableData = response.data.data;
          console.log(response.data.data);
          var data = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          $scope.sumBalance = data.reduce(function (sum, cur) {
            return sum + (cur.balance || 0);
          }, 0);

          $scope.sumSentMoney = data.reduce(function (sum1, cur) {
            return sum1 + (cur.sentMoney || 0);
          }, 0);

          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        })
      },

    };
  }

})();
