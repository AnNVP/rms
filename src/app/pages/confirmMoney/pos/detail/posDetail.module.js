(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.confirmMoney')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('confirmMoney.posDetail', {
          url: '/pos/{id}',
          title: 'POS Detail',
          templateUrl: 'app/pages/confirmMoney/pos/detail/posDetail.html',
          controller: 'PosDetailPageCtrl',
          'permission': 'Cashs.GetListCashTypePos' 
        });
    }
  
  })();
  