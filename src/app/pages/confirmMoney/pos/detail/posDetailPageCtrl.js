(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .controller('PosDetailPageCtrl', PosDetailPageCtrl);

  /** @ngInject */
  function PosDetailPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, notificationService, $ngConfirm) {
    $scope.posDetail = {
      //posReceipts: [],
      selection: [],
      form: {
        'unextractedSMS': {
          'adjustedMoney': null,
        'reasonAjustMoney': null,
        },
        'posReceipts': [],
        'sumAmountMoney': 0,
        // 'chechlech': null,
      },
      isS1: false,
      confirmOrNot: null,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      init: function () {
        var _this = this;
        _this.listKhuVuc = [
          { "id": 1, "name": "Native Thái Lan" },
          { "id": 2, "name": "Native Việt Nam" },
          { "id": 3, "name": "Native Indo" },
          { "id": 4, "name": "Hà Nội" },
          { "id": 5, "name": "Sài Gòn" },
        ];
        _this.listTransactionStatus = [
          { "id": "S1", "name": "S1_CHUA_XU_LY" },
          { "id": "S2", "name": "S2_KHOP" },
          { "id": "S3", "name": "S3_DA_DOI_SOAT" },
          { "id": "S4", "name": "S4_DA_DOI_SOAT_VA_KHOP" },
          { "id": "S8", "name": "S8_HOAN_THANH" },
        ];
        _this.listTypeOfTransaction = [
          // { "id": 110, "name": "110_KH_DT" },
          // { "id": 120, "name": "120_KH_DT_CTT" },
          // { "id": 210, "name": "210_KH_Topica_DP" },
          { "id": 220, "name": "220_KH_Topica_HQ" },
          // { "id": 230, "name": "230_KH_Topica_HQ_POS" },
          // { "id": 240, "name": "240_KH_Topica_HQ_TM" },
          { "id": 310, "name": "310_COD_Topica_HQ" },
          { "id": 320, "name": "320_CTT_Topica" },
          { "id": 330, "name": "330_Topica_SN_Topica_HQ" },
          { "id": 340, "name": "340_POS" },
          // { "id": 400, "name": "400_Topica_KH" },
          { "id": 500, "name": "500_DIEU_CHINH_GIAM_TRU" },
          { "id": 600, "name": "600_KHONG_GHI_NHAN" },
        ];
        apiService.get('smss/' + $stateParams.id, null, function (response) {
          _this.form.unextractedSMS = response.data;
          if (_this.form.unextractedSMS.transactionStatus == "S1") {
            _this.isS1 = true;
          }
          else {
            _this.isS1 = false;
          }
          _this.form.unextractedSMS.transactionStatusObject = _.filter(_this.listTransactionStatus, function (element) { return element.id == _this.form.unextractedSMS.transactionStatus })[0];
          var param = {};
          param.bank = _this.form.unextractedSMS.bank;
          param.bankAccountNo = _this.form.unextractedSMS.bankAccountNo;
          param.sentTime = _this.form.unextractedSMS.sentTime;
          var _sumAmountMoney = 0;
          if (_this.form.unextractedSMS.transactionStatus != "S4" && _this.form.unextractedSMS.transactionStatus != "S8") {
            apiService.get('cashs/pos', param, function (response) {
              var data = response.data;
              _.forEach(data, function (item) {
                item.checked = false;
                item.khuVucObject = _.filter(_this.listKhuVuc, function (element) { return element.id == item.khuVuc })[0];
                item.transactionStatusObject = _.filter(_this.listTransactionStatus, function (element) { return element.id == item.transactionStatus })[0];
              });
              _this.form.posReceipts = data;
              _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
            });
          }
          else {
            apiService.get('cashs/by-chose-in-mapping-data-in-confirm-money-incoming/' + $stateParams.id, null, function (response) {
              var data = response.data;
              _.forEach(data, function (item) {
                _this.selection.push(item.id);
                item.checked = false;
                _sumAmountMoney += item.soTienNop;
              });
              _this.form.posReceipts = data;
              _this.form.sumAmountMoney = _sumAmountMoney;
              _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
            });
          }

        });
      },

      back: function () {
        $state.go('confirmMoney.pos');
      },
      reload: function () {
        $state.reload();
      },

      toggleSelection: function (posId) {
        debugger
        var idx = this.selection.indexOf(posId);
        var posReceipt = _.find(this.form.posReceipts, function (o) {
          return posId == o.id;
        });
        if (idx > -1) {
          this.selection.splice(idx, 1);
          posReceipt.checked = false;
          this.form.sumAmountMoney -= posReceipt.soTienNop;
        } else {
          this.selection.push(posId);
          posReceipt.checked = true;
          this.form.sumAmountMoney += posReceipt.soTienNop;
        }
        // this.form.adjustedMoney = this.form.unextractedSMS.sentMoney - this.form.sumAmountMoney;
        this.form.unextractedSMS.adjustedMoney = this.form.unextractedSMS.sentMoney - this.form.sumAmountMoney;
      },
      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        var _this = this;
        // $scope.all = this;
        $scope.posDetail.confirmOrNot = confirmOrNot;
        if (_this.form.unextractedSMS.adjustedMoney > 0 || _this.form.unextractedSMS.adjustedMoney < 0) {
          $scope.chenhlech = _this.form.unextractedSMS.adjustedMoney;

          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + (confirmOrNot > 0 ? '' : "HỦY ") + titleName + '</p>',
            content: confirmOrNot > 0 ? 'Số tiền chênh lệch: <strong>{{chenhlech |number:0}} đ</strong>. Bạn có chắc chắc xác nhận không' : 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      correctSingleData: function ($scope) {
        var _this = $scope.posDetail;
        var params = {};
        params.cashIds = [];
        params.smsUnextractedId = $stateParams.id;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 2;

        apiService.update('smss/correct-single-bank-transfer-cash-incoming-money', params, function (response) {
          _this.reload();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      confirmSingleData: function ($scope) {
        var _this = $scope.posDetail;
        var params = {};
        params.cashIds = _this.selection;
        params.smsUnextractedId = $stateParams.id;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 2;
        if (_this.selection.length === 0 && params.confirmOrUnconfirm == 1) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        apiService.update('smss/confirm-mapping-single-bank-transfer-cash-incoming-money', params, function (response) {
          _this.reload();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6, hcm=7
      lock: function ($scope) {
        var _this = $scope.posDetail;
        var params = {};
        params.cashIds = _this.selection;
        params.smsUnextractedId = $stateParams.id;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.typeOfPayment = 2;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        apiService.update('smss/lock-bank-transfer-cash-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      save: function () {
        var _this = this;
        var param = {};
        param.id = _this.form.unextractedSMS.id;
        param.typeOfTransaction = _this.form.unextractedSMS.typeOfTransaction;
        apiService.update('smss/save-confirm-money', param, function (response) {
          _this.back();
        });
      },

    };
  }

})();