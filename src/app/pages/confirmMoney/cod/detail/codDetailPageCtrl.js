(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .controller('codDetailPageCtrl', codDetailPageCtrl);

  /** @ngInject */
  function codDetailPageCtrl($scope, $state, $stateParams, apiService, notificationService, $ngConfirm) {
    $scope.codDetail = {
      form: {
        'unextractedSMS': {
          'adjustedMoney': null,
        'reasonAjustMoney': null,
        },
        'codDetails': [],
        'sumAmountMoney': 0,
      },
      confirmOrNot : null,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      isS1: false,
      selection: [],
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var codDetail = _.filter(this.form.codDetails, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          this.selection.splice(idx, 1);
          codDetail.checked = false;
          this.form.sumAmountMoney -= codDetail.actualReceivedMoney;
        } else {
          this.selection.push(id);
          codDetail.checked = true;
          this.form.sumAmountMoney += codDetail.actualReceivedMoney;
        }
        // this.form.adjustedMoney = this.form.sumAmountMoney -
        //   this.form.unextractedSMS.sentMoney;
        this.form.unextractedSMS.adjustedMoney = this.form.unextractedSMS.sentMoney - this.form.sumAmountMoney;
      },
      back: function () {
        $state.go('confirmMoney.cod');
      },
      reload: function () {
        $state.reload();
      },
      init: function () {
        var _this = this;
        _this.listTransactionStatus = [
          { "id": "S1", "name": "S1_CHUA_XU_LY" },
          { "id": "S2", "name": "S2_KHOP" },
          { "id": "S3", "name": "S3_DA_DOI_SOAT" },
          { "id": "S4", "name": "S4_DA_DOI_SOAT_VA_KHOP" },
          { "id": "S8", "name": "S5_HOAN_THANH" },
        ];
        _this.listTypeOfTransaction = [
          // { "id": 110, "name": "110_KH_DT" },
          // { "id": 120, "name": "120_KH_DT_CTT" },
          // { "id": 210, "name": "210_KH_Topica_DP" },
          { "id": 220, "name": "220_KH_Topica_HQ" },
          // { "id": 230, "name": "230_KH_Topica_HQ_POS" },
          // { "id": 240, "name": "240_KH_Topica_HQ_TM" },
          { "id": 310, "name": "310_COD_Topica_HQ" },
          { "id": 320, "name": "320_CTT_Topica" },
          { "id": 330, "name": "330_Topica_SN_Topica_HQ" },
          { "id": 340, "name": "340_POS" },
          // { "id": 400, "name": "400_Topica_KH" },
          { "id": 500, "name": "500_DIEU_CHINH_GIAM_TRU" },
          { "id": 600, "name": "600_KHONG_GHI_NHAN" },
        ];
        var sumAmountMoney = 0;
        this.form.unextractedSMS.adjustedMoney = this.form.unextractedSMS.sentMoney - this.form.sumAmountMoney;
       
        apiService.get('smss/' + $stateParams.codId, null, function (response) {
          _this.form.unextractedSMS = response.data;
          if (_this.form.unextractedSMS.transactionStatus == "S1") {
            _this.isS1 = true;
          }
          else {
            _this.isS1 = false;
          }
          _this.form.unextractedSMS.transactionStatusObject = _.filter(_this.listTransactionStatus, function (element) {
            return element.id == _this.form.unextractedSMS.transactionStatus;
          })[0];
          var _sumAmountMoney = 0;

          if(_this.form.unextractedSMS.transactionStatus != "S4" && _this.form.unextractedSMS.transactionStatus != "S8")
          {
            _this.selection = [];
            var orderCodeCheck = _this.form.unextractedSMS.orderCode;
            if(orderCodeCheck != null && _this.form.unextractedSMS.orderLevel ==1)
            {
              _sumAmountMoney = 0;
              apiService.get('cods/cods-by-not-confirm/' + $stateParams.codId, null, function (response) {
                var data = response.data;
                  _.forEach(data, function (item) {
                    _this.selection.push(item.id);
                  item.checked = true;
                  _sumAmountMoney += item.actualReceivedMoney;
                  });
                _this.form.sumAmountMoney = _sumAmountMoney;
                _this.form.codDetails = data;
                _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
              });
            }
            else 
            {
              apiService.get('cods/viettel-cods-by-not-confirm/' + $stateParams.codId, null, function (response) {
                var data = response.data;
             
                _this.form.sumAmountMoney = _sumAmountMoney;
                _this.form.codDetails = data;
                _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
              });
            }
          }
          else
          {
            _this.selection = [];
            apiService.get('cods/by-chose-in-mapping-data-in-confirm-money-incoming/' + $stateParams.codId , null, function (response) {
              var data = response.data;
              _.forEach(data, function(item) {
                  _this.selection.push(item.id);
                  item.checked = false;
                  _sumAmountMoney += item.actualReceivedMoney;
              });
              _this.form.sumAmountMoney = _sumAmountMoney;
              _this.form.codDetails = data;
              _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
            });
          }
        })

      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        var _this = this;
        // $scope.all = this;
        $scope.codDetail.confirmOrNot = confirmOrNot;
        if (_this.form.unextractedSMS.adjustedMoney > 0 || _this.form.unextractedSMS.adjustedMoney < 0) {
          $scope.chenhlech = _this.form.unextractedSMS.adjustedMoney;

          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + (confirmOrNot > 0 ? '' : "HỦY ") + titleName + '</p>',
            content: confirmOrNot > 0 ? 'Số tiền chênh lệch: <strong>{{chenhlech |number:0}} đ</strong>. Bạn có chắc chắc xác nhận không' : 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },


      lock: function ($scope) {
        var _this = $scope.codDetail;

        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        var params = {};
        params.codIds = _this.selection;
        params.smsUnextractedId = $stateParams.codId;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        // if(_this.form.unextractedSMS.sentReason.toLowerCase().includes("viettel"))
        // params.isCodViettel = 1;
        // else  params.isCodViettel = 0;

        apiService.update('smss/lock-cod-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      confirmSingleData: function ($scope) {
        var _this = $scope.codDetail;
        var params = {};
        
        params.smsUnextractedId = $stateParams.codId;
        params.codIds = _this.selection;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 4;
        
        // if(_this.form.unextractedSMS.sentReason.toLowerCase().includes("viettel"))
        // params.isCodViettel = 1;
        // else  params.isCodViettel = 0;

        if (_this.selection.length === 0 && _this.confirmOrNot == 1 ) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        apiService.update('smss/confirm-mapping-single-sms-cod-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      correctSingleData: function ($scope) {
        var _this = $scope.codDetail;
        var params = {};
        params.smsUnextractedId = $stateParams.codId;
        params.codIds = [];
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 4;
        // if (_this.selection.length === 0 && _this.confirmOrNot == 1 ) {
        //   notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
        //   return false;
        // }
        // if(_this.form.unextractedSMS.sentReason.toLowerCase().includes("viettel"))
        // params.isCodViettel = 1;
        // else  params.isCodViettel = 0;

        apiService.update('smss/correct-single-sms-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      save: function () {
        var _this = this;
        var param = {};
        param.id = _this.form.unextractedSMS.id;
        param.typeOfTransaction = _this.form.unextractedSMS.typeOfTransaction;
        apiService.update('smss/save-confirm-money', param, function (response) {
          _this.back();
        });
      },

    };
  }

})();
