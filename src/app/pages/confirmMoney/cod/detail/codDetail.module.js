(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.confirmMoney')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('confirmMoney.codDetail', {
          url: '/confirm-money-cod/{codId}',
          title: 'COD_DETAIL',
          templateUrl: 'app/pages/confirmMoney/cod/detail/codDetail.html',
          controller: 'codDetailPageCtrl',
          'permission': 'Smss.GetById'
        });
    }
  
  })();
  