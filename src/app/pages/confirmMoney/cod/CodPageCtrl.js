(function () {
    'use strict';

    angular.module('BlurAdmin.pages.confirmMoney')
        .controller('CofirmMoneyCodPageCtrl', CofirmMoneyCodPageCtrl);

    /** @ngInject */
    function CofirmMoneyCodPageCtrl($scope, apiService) {

        $scope.cod = {
            isLoading: true,
            tableState: null,
            tablePageSize: 10,
            tableData: [],
            isInit: false,
            default: {
                search: {
                    startDate: moment().startOf('month'),
                    endDate: moment().endOf('month'),
                    orderCode: '',
                }
            },
            transactionStatus: [
                { value: '', label: 'Tất cả' },
                { value: 'S1', label: 'S1' },
                { value: 'S2', label: 'S2' },
                { value: 'S3', label: 'S3' },
                { value: 'S4', label: 'S4' },
                { value: 'S8', label: 'S8' },
            ],
            bankLabel: {
                'Techcombank': 'TECH',
                'VietinBank': 'VIETIN',
                'BIDV': 'BIDV',
                'Vietcombank': 'VIETCOM',
                'ACB': 'ACB',
                'Sacombank': 'SACOM',
                'AGRIBANK': 'AGR',
                'VPBank': 'VP'
            },
            myTransaction: [
                { value: '', label: 'Tất cả' },
                { value: 'IS_NOT_NULL', label: 'Của tôi' },
                { value: 'IS_NULL', label: 'Chưa xác định' }
            ],
            init: function (tableState) {
                $scope.cod.tableState = tableState;
                $scope.cod.refresh();
            },
            refresh: function () {
                var _this = this;
                _this.isLoading = true;
                var searchInit = _this.tableState;
                if (!_this.isInit) {
                    searchInit.all = {};
                    searchInit.all['checkbox'] = [{ orderCode: _this.default.search.orderCode }];
                    searchInit.all['date'] = [
                        { type: 'greater', data: { sentTime: _this.default.search.startDate.toDate() } },
                        { type: 'lesser', data: { sentTime: _this.default.search.endDate.toDate() } }
                    ];
                }
                _this.isInit = true;
                apiService.get('smss/extracted-sms-type-cod', searchInit, function (response) {
                    _this.tableData = response.data.data;
                    var data = response.data.data;
                    _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
                    _this.tableState.pagination.totalItemCount = response.data.totalRecords;
                    _this.isLoading = false;
                    $scope.sumSentMoney = data.reduce(function (sum, cur) {
                        return sum + (cur.sentMoney || 0);
                    }, 0);
                    $scope.sumBalance = data.reduce(function (sum1, cur) {
                        return sum1 + (cur.balance || 0);
                    }, 0);
                }, function () {
                    _this.isLoading = false;
                });

            }
        };
    }

})();