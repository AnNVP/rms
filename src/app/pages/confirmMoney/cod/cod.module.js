(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.confirmMoney')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('confirmMoney.cod', {
          url: '/confirm-money-cod',
          title: 'CODS',
          templateUrl: 'app/pages/confirmMoney/cod/cod.html',
          controller: 'CofirmMoneyCodPageCtrl',
          sidebarMeta: {
            order: 3,
          },
          'permission': 'Smss.GetListSmsTypeCod'
        });
    }
  
  })();
  