(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('confirmMoney', {
        url: '/confirm-money',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'CONFIRM_MONEY',
        sidebarMeta: {
          icon: 'ion-cash',
          order: 2,
        },
        'permission': ['Smss.GetUnextractedSMSTypePaymentGateway','Smss.GetListSmsTypePos','Smss.GetListSmsTypeCod',
        'Smss.GetUnextractedSMSTypePhieuThuDiaPhuong']
      });
  }

})();
