(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('confirmMoney.paymentGatewayDetail', {
        url: '/confirm-money-incoming-payment-gateways/{paymentGatewayId}',
        title: 'PAYMENT_GATEWAY',
        templateUrl: 'app/pages/confirmMoney/paymentGateway/detail/payment-gateway.html',
        controller: 'ConfirmMoneyPaymentGatewayPageCtrl',
        'permission': 'Smss.GetById'
      });
  }

})();
