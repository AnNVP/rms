(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .controller('ConfirmMoneyPaymentGatewayPageCtrl', ConfirmMoneyPaymentGatewayPageCtrl);

  /** @ngInject */
  function ConfirmMoneyPaymentGatewayPageCtrl($scope, $state, $stateParams, apiService, notificationService, $ngConfirm, permissionService) {
    $scope.paymentGateway = {
      form: {
        'unextractedSMS': {
          'adjustedMoney': null,
          'reasonAjustMoney': null,
        },
        'paymentGateways': [],
        'sumAmountMoney': 0,

        // 'chechlech': null,
      },
      isS1: false,
      confirmOrNot: null,
      // checkPermission: false,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      selection: [],
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var paymentGateway = _.filter(this.form.paymentGateways, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          this.selection.splice(idx, 1);
          paymentGateway.checked = false;
          this.form.sumAmountMoney -= paymentGateway.amountMoney;
        } else {
          this.selection.push(id);
          paymentGateway.checked = true;
          this.form.sumAmountMoney += paymentGateway.amountMoney;
        }
        this.form.unextractedSMS.adjustedMoney = this.form.unextractedSMS.sentMoney - this.form.sumAmountMoney;
      },
      back: function () {
        $state.go('confirmMoney.paymentGateways');
      },
      reload: function () {
        $state.reload();
      },
      init: function () {
        var _this = this;
        _this.listTransactionStatus = [
          { "id": "S1", "name": "S1_CHUA_XU_LY" },
          { "id": "S2", "name": "S2_KHOP" },
          { "id": "S3", "name": "S3_DA_DOI_SOAT" },
          { "id": "S4", "name": "S4_DA_DOI_SOAT_VA_KHOP" },
          { "id": "S8", "name": "S5_HOAN_THANH" },
        ];
        _this.listTypeOfTransaction = [
          { "id": 220, "name": "220_KH_Topica_HQ" },
          { "id": 310, "name": "310_COD_Topica_HQ" },
          { "id": 320, "name": "320_CTT_Topica" },
          { "id": 330, "name": "330_Topica_SN_Topica_HQ" },
          { "id": 340, "name": "340_POS" },
          { "id": 500, "name": "500_DIEU_CHINH_GIAM_TRU" },
          { "id": 600, "name": "600_KHONG_GHI_NHAN" },
        ];
        var sumAmountMoney = 0;
        _this.selection = [];
        _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
        apiService.get('smss/paygate/' + $stateParams.paymentGatewayId, null, function (response) {
          _this.form.unextractedSMS = response.data;
          _this.form.unextractedSMS.adjustedMoney = response.data.disparityMoney;
          if (_this.form.unextractedSMS.transactionStatus == "S1") {
            _this.isS1 = true;
          }
          else {
            _this.isS1 = false;
          }
          _this.form.unextractedSMS.transactionStatusObject = _.filter(_this.listTransactionStatus, function (element) {
            return element.id == _this.form.unextractedSMS.transactionStatus;
          })[0];
          // _this.checkPermission = false;
          // _this.checkPermission = permissionService.isAllowed(['KTKH_HN']);
          if (_this.form.unextractedSMS.transactionStatus != "S4" && _this.form.unextractedSMS.transactionStatus != "S8") {
            var time1 = moment(_this.form.unextractedSMS.time1);
            var time2 = moment(_this.form.unextractedSMS.time2);
            // apiService.get('smss/unextracted-sms-type-payment-gateway/previous/' + $stateParams.paymentGatewayId, null, function (response) {
              // var previousSentTime = (response.data.sentTime ? moment(response.data.sentTime) : moment(665971200000));
              apiService.get('paymentgateways/by-unextracted-sms-not-confirm/' + $stateParams.paymentGatewayId, null, function (response1) {
                var data = response1.data;
                _.forEach(data, function (item) {
                  item.checked = false;
                  if (moment(item.timeOfPayment).isBetween(time1, time2)) {
                    _this.selection.push(item.id);
                    item.checked = true;
                    sumAmountMoney += item.amountMoney;
                  }
                });
                _this.form.paymentGateways = data;
                _this.form.sumAmountMoney = sumAmountMoney;
                _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
              });
            // });
          }
          else {
            apiService.get('paymentgateways/by-chose-in-mapping-data-in-confirm-money-incoming/' + $stateParams.paymentGatewayId, null, function (response) {
              var data = response.data;
              _.forEach(data, function (item) {
                _this.selection.push(item.id);
                item.checked = false;
                sumAmountMoney += item.amountMoney;
              });
              _this.form.paymentGateways = data;
              _this.form.sumAmountMoney = sumAmountMoney;
              _this.form.unextractedSMS.adjustedMoney = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney;
            });
          }
        });
      },

      // changeTienChenhLech: function () {
      //   var _this = this;
      //   _this.form.chechlech = _this.form.unextractedSMS.sentMoney - _this.form.sumAmountMoney - _this.form.adjustedMoney;
      // },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        var _this = this;
        // $scope.all = this;
        $scope.paymentGateway.confirmOrNot = confirmOrNot;
        if (_this.form.unextractedSMS.adjustedMoney > 0 || _this.form.unextractedSMS.adjustedMoney < 0) {
          $scope.chenhlech = _this.form.unextractedSMS.adjustedMoney;

          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: confirmOrNot > 0 ? 'Số tiền chênh lệch: <strong>{{chenhlech |number:0}} đ</strong>. Bạn có chắc chắc xác nhận không' : 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      lock: function ($scope) {
        var _this = $scope.paymentGateway;
        var params = {};
        params.paymentGatewayIds = _this.selection;
        params.smsUnextractedId = $stateParams.paymentGatewayId;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        apiService.update('smss/lock-payment-gateway-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      confirmSingleData: function ($scope) {
        var _this = $scope.paymentGateway;

        var params = {};
        params.paymentGatewayIds = _this.selection;
        params.smsUnextractedId = $stateParams.paymentGatewayId;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 3;

        if (_this.selection.length === 0 && _this.confirmOrNot == 1) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        apiService.update('smss/confirm-mapping-single-sms-pg-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      //bankTransfer = 1, Pos = 2,Paygate = 3,Cod = 4,cash = 5,atm = 6
      correctSingleData: function ($scope) {
        var _this = $scope.paymentGateway;
        var params = {};
        params.paymentGatewayIds = [];
        params.smsUnextractedId = $stateParams.paymentGatewayId;
        params.adjustedMoney = _this.form.unextractedSMS.adjustedMoney;
        params.reasonAjustMoney = _this.form.unextractedSMS.reasonAjustMoney;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        params.typeOfPayment = 3;
        apiService.update('smss/correct-single-sms-incoming-money', params, function (response) {
          _this.selection = [];
          _this.back();
        });
      },

      save: function () {
        var _this = this;
        var param = {};
        param.id = _this.form.unextractedSMS.id;
        param.typeOfTransaction = _this.form.unextractedSMS.typeOfTransaction;
        apiService.update('smss/save-confirm-money', param, function (response) {
          _this.back();
        });
      },

    };
  }

})();
