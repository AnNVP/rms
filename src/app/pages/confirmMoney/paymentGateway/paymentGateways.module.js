(function () {
  'use strict';

  angular.module('BlurAdmin.pages.confirmMoney')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('confirmMoney.paymentGateways', {
        url: '/confirm-money-incoming-payment-gateways',
        title: 'PAYMENT_GATEWAY',
        templateUrl: 'app/pages/confirmMoney/paymentGateway/payment-gateways.html',
        controller: 'ConfirmMoneyPaymentGatewaysPageCtrl',
        sidebarMeta: {
          order: 0,
        },
        'permission': 'Smss.GetUnextractedSMSTypePaymentGateway'
      });
  }

})();
