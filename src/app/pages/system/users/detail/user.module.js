(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.userDetail', {
        url: '/users/{userId}',
        title: 'USERS',
        templateUrl: 'app/pages/system/users/detail/user.html',
        controller: 'UserPageCtrl',
        'permission': 'Users.GetById'
      });
  }

})();
