(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .controller('UserPageCtrl', UserPageCtrl);

  /** @ngInject */
  function UserPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate) {
    $scope.user = {
      isCreateMode: _.isEmpty($stateParams.userId),
      isEdit: !_.isEmpty($stateParams.userId),
      isSave: _.isEmpty($stateParams.userId),
      routes: [],
      groups: [],
      selectionRoute: [],
      selectionGroup: [],
      form: {},
      back: function () {
        $state.go('system.users');
      },
      reload: function () {
        $state.reload();
      },
      showEdit: function() {
        var _this = this;
        _this.isEdit = false;
        $timeout(function() {
          _this.isSave = true;
        }, 200);
      },
      initRoutes: function() {
        var _this = this;
        apiService.get('routes', null, function (response) {
          console.log(response.data);
          _.forEach(response.data, function(route) {
            $translate(route.name).then(function (messageTranslated) {
              route.name = messageTranslated;
              _this.routes.push(route);
            }, function (translationId) {
              route.name = translationId;
              _this.routes.push(route);
            });
          });
        });
      },
      initGroups: function () {
        var _this = this;
        apiService.get('groups/all', null, function (response) {
          _this.groups = response.data;
        });
      },
      toggleSelectionRoute: function (routeId) {
        var idx = this.selectionRoute.indexOf(routeId);
        if (idx > -1) {
               console.log(idx);
               console.log(routeId);
          this.selectionRoute.splice(idx, 1);
        } else {
          this.selectionRoute.push(routeId);
        }
      },
      toggleSelectionGroup: function (groupId) {
        var before = _.clone(this.selectionGroup);
        var idx = this.selectionGroup.indexOf(groupId);
        if (idx > -1) {
     
          this.selectionGroup.splice(idx, 1);
        } else {
          this.selectionGroup.push(groupId);
        }
        this.reloadSelectionRoute(before);
      },
      reloadSelectionRoute: function (before) {
        var _this = this;
        var routes = _this.getRoutesFromGroup(_this.selectionGroup);
        var routesBefore = _this.getRoutesFromGroup(before);
        var diff = _.difference(_this.selectionRoute, routesBefore);
        _this.selectionRoute = _.union(_.concat(diff, routes));
      },
      getRoutesFromGroup: function(group) {
        var _this = this;
        var groupsSelected = _.filter(_.clone(_this.groups), function (o) {
          return group.indexOf(o.id) !== -1;
        });
        var groupRoutes = _.flatMap(groupsSelected, function (o) { return o.groupRoutes; });
        var routes = _.union(_.flatMap(groupRoutes, function (o) { return o.routeId; }));
        return routes;
      },
      init: function() {
        var _this = this;
        _this.initGroups();
        _this.initRoutes();
        if (!_this.isCreateMode) {
          apiService.get('users/' + $stateParams.userId, null, function (response) {
            _this.form = response.data;
            _this.selectionGroup = _.map(response.data.userGroups, function (o) {
              return o.group.id;
            });
            _this.selectionRoute = _.map(response.data.userRoutes, function (o) {
              return o.route.id;
            });
          });
        }
      },
      save: function () {
        var _this = this;
        var data = _.clone(_this.form);
        data.userGroups = [];
        _.forEach(_this.selectionGroup, function (groupId) {
          data.userGroups.push({
            GroupId: groupId
          });
        })
        data.userRoutes = [];
        _.forEach(_this.selectionRoute, function (routeId) {
          data.userRoutes.push({
            IdRoute: routeId,
            CreatedAt: moment()
          });
        })
        if (_this.isCreateMode) {
          apiService.create('users', data, function (response) {
            _this.back();
          });
        } else {
          apiService.update('users/' + $stateParams.userId, data, function (response) {
            _this.reload();
          });
        }
      }
    };
  }

})();
