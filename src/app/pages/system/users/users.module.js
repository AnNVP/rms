(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.users', {
        url: '/users',
        title: 'USERS',
        templateUrl: 'app/pages/system/users/users.html',
        controller: 'UsersPageCtrl',
        sidebarMeta: {
          order: 0,
        },
        'permission': 'Users.GetAll'
      });
  }

})();
