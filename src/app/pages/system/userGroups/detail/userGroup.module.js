(function () {
  'use strict';

  angular.module('BlurAdmin.pages.system')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('system.userGroupDetail', {
        url: '/user-groups/{userGroupId}',
        title: 'USER_GROUPS',
        templateUrl: 'app/pages/system/userGroups/detail/userGroup.html',
        controller: 'UserGroupPageCtrl',
        'permission': 'Groups.GetById'
      });
  }

})();
