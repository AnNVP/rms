(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_Global', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_Global', {
        url: '/PowerBi_Global',
        templateUrl: 'app/pages/PowerBi_Global/PowerBi_Global.html',
        title: 'Báo cáo SSRS GLobal',
        sidebarMeta: {
          icon: 'icon ion-cash',
          order: 1,
        },
      });
  }

})();