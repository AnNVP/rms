/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.morris')
    .controller('morrisCtrl', morrisCtrl);

  /** @ngInject */
  function morrisCtrl($scope, $window,apiService, baConfig) {
    $scope.$watch('date', function (newValue, oldValue) {
      loadCharts();
    })
    
    $scope.$watch('datePicker', function (newValue, oldValue) {
      loadCharts();
    })

    function loadCharts() {
      var params = {
        ngayBatDau: $scope.datePicker.startDate.toDate(),
        ngayKetThuc: $scope.datePicker.endDate.toDate()
      }
      apiService.get('orders', params, function (response) {
    var layoutColors = baConfig.colors;
    $scope.colors = [layoutColors.primary, layoutColors.warning, layoutColors.danger, layoutColors.info, layoutColors.success, layoutColors.primaryDark];
      $scope.lineData = [];
      if (response.data.length > 0)
      {
      for (var i = 0; i < response.data.length; i++)
    {
        $scope.lineData.push({ y: response.data[i].ngay, a: response.data[i].soLuongGiaoDich, b: response.data[i].tongTienGiaoDich});
    }  
      }
      else 
      {
        $scope.lineData.push({ y: 0, a: 0});
      }

    });

    // $scope.areaData = [
    //   {y: "2006", a: 100, b: 90},
    //   {y: "2007", a: 75, b: 65},
    //   {y: "2008", a: 50, b: 40},
    //   {y: "2009", a: 75, b: 65},
    //   {y: "2010", a: 50, b: 40},
    //   {y: "2011", a: 75, b: 65},
    //   {y: "2012", a: 100, b: 90}
    // ];
    // $scope.barData = [
    //   {y: "2006", a: 100, b: 90},
    //   {y: "2007", a: 75, b: 65},
    //   {y: "2008", a: 50, b: 40},
    //   {y: "2009", a: 75, b: 65},
    //   {y: "2010", a: 50, b: 40},
    //   {y: "2011", a: 75, b: 65},
    //   {y: "2012", a: 100, b: 90}
    // ];
    // $scope.donutData = [
    //   {label: "Download Sales", value: 12},
    //   {label: "In-Store Sales", value: 30},
    //   {label: "Mail-Order Sales", value: 20}
    // ];

    angular.element($window).bind('resize', function () {
      // $window.Morris.Grid.prototype.redraw();
    });
  }
}
    
})();