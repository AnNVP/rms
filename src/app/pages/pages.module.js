/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [
      'ui.router',
      //'BlurAdmin.pages.dashboard',
      // 'BlurAdmin.pages.ui',
      // 'BlurAdmin.pages.invoices',
      // 'BlurAdmin.pages.orders',
      // 'BlurAdmin.pages.products',
      // 'BlurAdmin.pages.packages',
      // 'BlurAdmin.pages.components',
      // 'BlurAdmin.pages.form',
      // 'BlurAdmin.pages.tables',
      // 'BlurAdmin.pages.charts',
      // 'BlurAdmin.pages.maps',
      'BlurAdmin.pages.profile',
      'BlurAdmin.pages.system',
      //'BlurAdmin.pages.confirmMoney',
      //'BlurAdmin.pages.charts.chartJs',
      //'BlurAdmin.pages.charts.morris',
      //'BlurAdmin.pages.charts.amCharts',
      //'BlurAdmin.pages.reports',
      //'BlurAdmin.pages.transactionManagement',
      //'BlurAdmin.pages.crossChecks',
      // 'BlurAdmin.pages.powerBI',
      'BlurAdmin.pages.PowerBi_Global',
      'BlurAdmin.pages.PowerBi_EVN',
      'BlurAdmin.pages.PowerBi_NTL',
      'BlurAdmin.pages.PowerBi_NVN',
      'BlurAdmin.pages.PowerBi_TG',
      'BlurAdmin.pages.PowerBi_K3300',

      //'BlurAdmin.pages.dashboardPowerBI'
      //'BlurAdmin.pages.moneyConfirmation'
    ])
  .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/PowerBi_Global');

    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Pages',
    //   icon: 'ion-document',
    //   subMenu: [{
    //     title: 'Sign In',
    //     fixedHref: 'auth.html',
    //     blank: true
    //   }, {
    //     title: 'Sign Up',
    //     fixedHref: 'reg.html',
    //     blank: true
    //   }, {
    //     title: 'User Profile',
    //     stateRef: 'profile'
    //   }, {
    //     title: '404 Page',
    //     fixedHref: '404.html',
    //     blank: true
    //   }]
    // });
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Menu Level 1',
    //   icon: 'ion-ios-more',
    //   subMenu: [{
    //     title: 'Menu Level 1.1',
    //     disabled: true
    //   }, {
    //     title: 'Menu Level 1.2',
    //     subMenu: [{
    //       title: 'Menu Level 1.2.1',
    //       disabled: true
    //     }]
    //   }]
    // });
  }

})();
