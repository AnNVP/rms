(function () {
  'use strict';

  angular.module('BlurAdmin.pages.powerBI', [])
        .config(routeConfig).config(function (baConfigProvider) {
          var layoutColors = baConfigProvider.colors;
        });

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('powerBI', {
        url: '/powerBI',
        templateUrl: 'app/pages/powerBI/powerBI.html',
        title: 'Power BI',
        controller: 'powerBICtrl',
        sidebarMeta: {
          icon: 'ion-folder',
          order: 1,
        },
      });
  }

})();