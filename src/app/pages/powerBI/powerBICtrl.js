(function () {
  'use strict';
  angular.module('BlurAdmin.pages.powerBI')
    .controller('powerBICtrl', powerBICtrl);
  /** @ngInject */
  function powerBICtrl($scope, apiService, confirmService, $window) {
    $scope.powerBI = {

      isLoading: true,
      btnDisabled: false,
      tableData: [],

      init: function () {
        $scope.checkReadOnly = false;
        this.refresh();
      },
      refresh: function () {
        var _this = this;
        apiService.get('oauth/info', null, function (response) {
          $scope.email = response.data.email;
          var params = {
            email: $scope.email,
            CreatedBy: $scope.email
          }
          _this.isLoading = true;
          apiService.get('PowerBI', params, function (response) {
            console.log(response.data.data.length);
            for (var i = 1; i < response.data.data.length; i++)
            {
            if (response.data.data[i].createdBy != $scope.email) {
              $scope.btnDisabled = true;
            } else {
              $scope.btnDisabled = false;
            }
            }
            _this.tableData = response.data.data;
            _this.isLoading = false;
            $scope.checkReadOnly = false;
          
          });
          
        });
      }
    };

    $scope.add = function () {
      $scope.addShow = true;
    };
    $scope.save = function (item) {
      var _this = this;
      var data = item;
      $scope.checkReadOnly = true;
      var id = data.id;
      data.ParentId = $scope.email;
      apiService.update('PowerBI/' + id, data, function (response) {
        $scope.powerBI.refresh();
      });
    }
    $scope.edit = function () {
      $scope.addShow = false;
      $scope.editModeInsert = false;
    }
    $scope.cancel = function () {
      $scope.addShow = false;
    }
    $scope.new = function (item) {
      apiService.get('oauth/info', null, function (response) {
        $scope.email = response.data.email;
        var params = {
          email: $scope.email,
          CreatedBy: $scope.email
        }
        $scope.checkReadOnly = false;
        var data = item;
        data.createdBy = $scope.email;
        $scope.addShow = false;
        if (item.tenBaoCao != null) {
          apiService.create('PowerBI', data, function (response) {
            $scope.powerBI.refresh();
          });
          apiService.get('PowerBI/get-id', null, function (response) {
            $scope.idInsert = response.data + 1;
            apiService.get('PowerBIPermissions/get-id', null, function (response) {
              $scope.idPermission = response.data + 1;
              data.ParentId = $scope.email;
              data.ReportId = $scope.idInsert;
              data.UserId = $scope.email;
              data.Id = $scope.idPermission;

              apiService.create('PowerBIPermissions', data, function (response) {});
              apiService.get('PowerBI', params, function (response) {
                $scope.powerBI.refresh();
                $timeout(function () {
                  $window.location.reload();
                  $scope.selected = [];
                }, 1000);
              });
            });
          });
        }
      });
    };
    $scope.removeRow = function (item) {
      confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
        var id = item.id;
        $scope.checkReadOnly = false;
        apiService.delete('PowerBI/' + id, function (response) {
          $scope.powerBI.refresh();
        });
      });
    }
    $scope.editPopUp = function (item) {}
  }
})();