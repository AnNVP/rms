(function () {
  'use strict';

  angular.module('BlurAdmin.pages.powerBI')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBIDetail', {
        url: '/PowerBIDetail/{PowerBIId}',
        title: 'POWER_BI_DETAILS',
        templateUrl: 'app/pages/powerBI/detail/PowerBIDetail.html',
        controller: 'PowerBIDetailPageCtrl',
      });
  }

})();
