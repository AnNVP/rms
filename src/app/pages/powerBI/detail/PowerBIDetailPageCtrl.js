(function () {
  'use strict';
  angular.module('BlurAdmin.pages.powerBI')
    .controller('PowerBIDetailPageCtrl', PowerBIDetailPageCtrl);
  /** @ngInject */
  function PowerBIDetailPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, $ngConfirm, confirmService, $window) {
    apiService.get('PowerBI/get-id', null, function (response) {
      $scope.idInsert = response.data + 1;
    });
    apiService.get('PowerBIPermissions/get-id', null, function (response) {
      $scope.idPermission = response.data + 1;
    });
    
    var params = {
      email: $scope.email,
      CreatedBy: $scope.email
    }
    $scope.PowerBIDetail = {
      isLoading: true,
      btnDisabled: false,
      // tablePageSize: 10,
      tableData: [],
      selection: [],
      selected: [],
      tableAssignTo: [],
      initAssignTo: function () {
        var _this = this;
        apiService.get('PowerBI/get-all-user', null, function (response) {
          _this.tableAssignTo = response.data;
        });
      },
      init: function () {
        $scope.checkReadOnly = false;
        this.refresh();
        this.initAssignTo();
      },
      toggleSelection: function (id) {
        var _this = this;
        var before = _.clone(this.selection);
        var idx = this.selection.indexOf(id);
        if (idx > -1) {
          this.selection.splice(idx, 1);
        } else {
          this.selection.push(id);
        }
        this.reloadSelection(before);
        _.forEach(_this.selection, function (Id) {
          $scope.AssignTo.push(Id);
        });
      },
      reloadSelection: function (before) {
        var _this = this;
        var diff = _.difference(_this.selection, $scope.dataEmail);
        _this.selectionUser = _.union(_.concat(diff, $scope.dataEmail));
        $scope.selected = [];
        $scope.selected = _.union(_.concat(diff, $scope.dataEmail))
      },
      back: function () {
        $state.go('powerBI');
      },
      f5: function () {
        $window.location.reload();
      },
      refresh: function () {
           var _this = this;
        apiService.get('oauth/info', null, function (response) {
          $scope.email = response.data.email
        _this.isLoading = true;
        apiService.get('PowerBI/get-data-by-id?id=' + $stateParams.PowerBIId, params, function (response) {
          console.log(response.data.data[0].createdBy);
         if (response.data.data[0].createdBy != $scope.email)
         {
           $scope.btnDisabled = true;
         }
         else
         {
              $scope.btnDisabled = false;
         }
          $scope.AssignTo = [];
          $scope.dataEmail = [];
          for (var i = 0; i < response.data.data.length; i++) {
            $scope.AssignTo.push(response.data.data[i].userId);
          }
          _this.tableData = response.data.data;
          _this.isLoading = false;
          $scope.checkReadOnly = false;
        });
         });
        var _this = this;
        apiService.get('PowerBI/get-all-user', null, function (response) {});
      },
    };
    $scope.add = function () {
      $scope.addShow = true;
    };
    $scope.save = function () {
      var _this = this;
      var data = item;
      $scope.checkReadOnly = true;
      var id = data.id;
      data.ParentId = $scope.email;
      apiService.update('PowerBIPermissions/' + id, data, function (response) {});
      $scope.PowerBIDetail.refresh();
      $scope.PowerBIDetail.f5();
    }
    $scope.edit = function () {
      $scope.addShow = false;
      $scope.editModeInsert = false;
    }
    $scope.cancel = function () {
      $scope.addShow = false;
    }
    $scope.new = function () {
      $scope.checkReadOnly = false;
      $scope.addShow = false;
      
      for (var i = 0; i < $scope.selected.length; i++) {
        apiService.get('PowerBIPermissions/get-id', null, function (response) {
          $scope.idPermission = response.data + 1;
          console.log($scope.idPermission);
        });
        var params = {
          UserId: $scope.selected[i],
          ParentId: $scope.email,
          ReportId: $stateParams.PowerBIId,
          Id: $scope.idPermission
        }
        apiService.create('PowerBIPermissions', params, function (response) {
          $timeout(function () {
            $window.location.reload();
            $scope.selected = [];
          }, 5000);
        });
      }
      $scope.PowerBIDetail.refresh();
      $scope.selected = [];
    };
    $scope.removeRow = function () {
      confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function () {
        $scope.checkReadOnly = false;
        for (var i = 0; i < $scope.selected.length; i++) {
          var params = {
            id: $stateParams.PowerBIId,
            Email: $scope.selected[i]
          }
          apiService.delete('PowerBIPermissions/' + $stateParams.PowerBIId + '/' + $scope.selected[i], params, function (response) {
            $scope.selected = [];
            $window.location.reload();
          });
        }
        apiService.get('PowerBI', params, function (response) {
          $scope.selected = [];
          console.log($scope.selected);
          $window.location.reload();
        });
      });
    }
  }
})();