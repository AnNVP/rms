(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.TMHNDetail', {
        url: '/manage-TMHNs/{TMHNId}',
        title: 'TIEN_MAT_HA_NOI',
        templateUrl: 'app/pages/transactionManagement/TMHNs/detail/TMHN.html',
        controller: 'TMHNPageCtrl',
      });
  }

})();
