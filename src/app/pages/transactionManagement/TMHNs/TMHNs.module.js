(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.TMHNs', {
        url: '/manage-TMHNs',
        title: 'TIEN_MAT_HA_NOI',
        templateUrl: 'app/pages/transactionManagement/TMHNs/TMHNs.html',
        controller: 'TMHNsPageCtrl',
        sidebarMeta: {
          order: 5,
        },
        'permission': 'Cashs.GetTMHN'
      });
  }

})();
