(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('POSPageCtrl', POSPageCtrl);

  /** @ngInject */
  function POSPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, $ngConfirm) {
    $scope.POS = {
      selection: [],
      confirmOrNot: null,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      form: {
        nguoiNopTien: null,
        ngayBienLai: null,
        tongTienPhaiThu: null,
        hinhThucThanhToan: 3,
        khuVuc: null,
        ngayThuTien: null,
        soTienNop: null,
        soTienBangChu: null,
        noiDung: null,
        status: 1,
        orderCode: null,
        orderId: null,
        donHang: null,
        soPhieu: null,
        email: null,
        phoneNumber: null,
        bank: null,
        order:
          {
            tongTienPhaiThu: null,
            tvts: null,
            orderDetails: [
              {
                orderId: null,
                orderCode: null,
                topicaUserId: null,
                packageId: null,
                hoVaTen: null,
                ngaySinh: null,
                sdt: null,
                email: null,
                soTien: null,
                trangThai: null,
                soTienNop: null,
                soTienNopHienTai: null
              }
            ],
          }
      },
      back: function () {
        $state.go('transactionManagement.POSs');
      },
      reload: function () {
        $state.reload();
      },
      init: function () {
        var _this = this;
        _this.listKhuVuc = [
          { "id": 4, "name": "Hà Nội" },
          { "id": 5, "name": "Sài Gòn" },
        ];
        _this.listNganHang = [
          { "id": 1, "name": "VCB283" },
          { "id": 2, "name": "VCB998" },
          { "id": 3, "name": "VT711" },
          { "id": 4, "name": "SCB" },
          { "id": 5, "name": "VT711_1" },
          { "id": 6, "name": "VT711_2" },
        ];
        _this.listTransactionStatus = [
          { "id": "S1", "name": "S1_CHUA_XU_LY" },
          { "id": "S2", "name": "S2_KHOP" },
          { "id": "S3", "name": "S3_DA_DOI_SOAT" },
          { "id": "S4", "name": "S4_DA_DOI_SOAT_VA_KHOP" },
          { "id": "S8", "name": "S8_HOAN_THANH" },
        ];
        apiService.get('cashs/' + $stateParams.POSId, null, function (response) {
          _this.form = response.data;
          // _this.form.order.orderStatusObject = _.filter(_this.listTrangThaiOrder, function (o) {
          //   return o.id == _this.form.order.orderStatus
          // })[0];
          // _this.form.transactionStatusObject = _.filter(_this.listTransactionStatus, function (o) {
          //   return o.id == _this.form.transactionStatus
          // })[0];
          _this.form.khuVucObject = _.filter(_this.listKhuVuc, function (o) { return o.id == response.data.khuVuc; })[0]
          _this.form.bankObject = _.filter(_this.listNganHang, function (o) { return o.id == response.data.bank; })[0]
        });
      },
      // confirm: function () {
      //   var _this = this;
      //   apiService.update('cashs/confirm-single-cash-transaction-manage/' + $stateParams.POSId, null, function (response) {
      //     _this.reload();
      //   });
      // },
      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.POS.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      correctSingleData: function ($scope) {
        var _this = $scope.POS;
        var params = {};
        params.CashIdSingle = $stateParams.POSId;
        params.confirmOrUnconfirm =_this.confirmOrNot;

        apiService.update('cashs/correct-single-cash-transaction-manage', params, function (response) {
          _this.reload();
        });
      },


      confirm: function ($scope) {
        var _this = $scope.POS;
        var params = {};
        params.CashIdSingle = $stateParams.POSId;
        params.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('cashs/confirm-single-cash-transaction-manage', params, function (response) {
          _this.reload();
        });
      },

    };
  }

})();
