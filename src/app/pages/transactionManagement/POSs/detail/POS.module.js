(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.POSDetail', {
        url: '/manage-POSs/{POSId}',
        title: 'POS',
        templateUrl: 'app/pages/transactionManagement/POSs/detail/POS.html',
        controller: 'POSPageCtrl',
      });
  }

})();
