(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.POSs', {
        url: '/manage-POSs',
        title: 'Tiền Mặt POS',
        templateUrl: 'app/pages/transactionManagement/POSs/POSs.html',
        controller: 'POSsPageCtrl',
        sidebarMeta: {
          order: 4,
        },
        'permission': 'Cashs.GetByPOS'
      });
  }

})();
