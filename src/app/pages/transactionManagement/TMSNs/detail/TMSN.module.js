(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.TMSNDetail', {
        url: '/manage-TMSNs/{TMSNId}',
        title: 'TIEN_MAT_SAI_GON',
        templateUrl: 'app/pages/transactionManagement/TMSNs/detail/TMSN.html',
        controller: 'TMSNPageCtrl',
      });
  }

})();
