(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('TMSNPageCtrl', TMSNPageCtrl);

  /** @ngInject */
  function TMSNPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, $ngConfirm, permissionService) {
    $scope.TMSN = {
      selection: [],
      confirmOrNot : null,
      //checkPermission: false,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      form: {
        nguoiNopTien: null,
        ngayBienLai: null,
        tongTienPhaiThu: null,
        hinhThucThanhToan: null,
        hinhThucThanhToanObject: null,
        khuVuc: null,
        ngayThuTien: null,
        soTienNop: null,
        soTienBangChu: null,
        noiDung: null,
        status: 1,
        orderCode: null,
        orderId: null,
        donHang: null,
        soPhieu: null,
        email: null,
        phoneNumber: null,
        bank: null,
        order:
          {
            tongTienPhaiThu: null,
            tvts: null,
            orderDetails: [
              {
                orderId: null,
                orderCode: null,
                topicaUserId: null,
                packageId: null,
                hoVaTen: null,
                ngaySinh: null,
                sdt: null,
                email: null,
                soTien: null,
                trangThai: null,
                soTienNop: null,
                soTienNopHienTai: null
              }
            ],
          }
      },
      back: function () {
        $state.go('transactionManagement.TMSNs');
      },
      reload: function () {
        $state.reload();
      },
      init: function () {
        var _this = this;
        _this.listHinhThucThanhToan = [
          { "id": 1, "name": "TMSN" },
          { "id": 2, "name": "TMSN" },
        ];
        _this.listKhuVuc = [
          { "id": 1, "name": "Native Thái Lan" },
          { "id": 2, "name": "Native Việt Nam" },
          { "id": 3, "name": "Native Indo" }
        ];
        // _this.listTransactionStatus = [
        //   { "id": "S1", "name": "S1_CHUA_XU_LY" },
        //   { "id": "S2", "name": "S2_KHOP" },
        //   { "id": "S3", "name": "S3_DA_DOI_SOAT" },
        //   { "id": "S4", "name": "S4_DA_DOI_SOAT_VA_KHOP" },
        //   { "id": "S8", "name": "S5_HOAN_THANH" },
        // ];
        apiService.get('cashs/' + $stateParams.TMSNId, null, function (response) {
          _this.form = response.data;
          // _this.form.order.orderStatusObject = _.filter(_this.listTrangThaiOrder, function(o){
          //   return o.id == _this.form.order.orderStatus
          // })[0];
          // _this.form.transactionStatusObject = _.filter(_this.listTransactionStatus, function(o){
          //   return o.id == _this.form.transactionStatus
          // })[0];
          _this.form.hinhThucThanhToanObject = _.filter(_this.listHinhThucThanhToan, function (o) { return o.id == response.data.hinhThucThanhToan; })[0]
          _this.form.khuVucObject = _.filter(_this.listKhuVuc, function (o) { return o.id == response.data.khuVuc; })[0]
          //_this.checkPermission = false;
          //_this.checkPermission = permissionService.isAllowed(['PM_KTKH','KTKH_HN']);
        });
      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.TMSN.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },
      
      correctSingleData: function ($scope) {
        var _this = $scope.TMSN;
        var params = {};
        params.CashIdSingle = $stateParams.TMSNId;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cashs/correct-single-cash-transaction-manage', params, function (response) {
          _this.reload();
        });
      },

      confirm: function ($scope) {
        var _this = $scope.TMSN;
        var params = {};
        params.CashIdSingle = $stateParams.TMSNId;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cashs/confirm-single-cash-transaction-manage', params, function (response) {
          _this.reload();
        });
      },

    };
  }

})();
