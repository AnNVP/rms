(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('TMSNsPageCtrl', TMSNsPageCtrl);

  /** @ngInject */
  function TMSNsPageCtrl($scope, apiService, confirmService, notificationService,$ngConfirm, permissionService) {

    $scope.TMSNs = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      selected: [],
      selection: [],
      confirmOrNot : null,
      //checkPermission: false,
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: 'IS_NOT_NULL'
        }
      },
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],
      init: function (tableState) {
        $scope.TMSNs.tableState = tableState;
        $scope.TMSNs.refresh();
      },
      refresh: function () {
 
        var _this = this;
        _this.isLoading = true;
        _this.checkStatus = true;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{orderCode:_this.default.search.orderCode}];
          searchInit.all['date'] = [
            {type:'greater', data:{ngayThuTien:_this.default.search.startDate.toDate()}},
            {type:'lesser', data:{ngayThuTien:_this.default.search.endDate.toDate()}}
          ];
        }
        _this.isInit = true;

        apiService.get('cashs/getTMSN', searchInit, function (response) {
          _this.tableData = response.data.data;
          var data =response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          //_this.checkPermission = false;
          //_this.checkPermission = permissionService.isAllowed(['PM_KTKH','KTKH_HN']);
          _this.isLoading = false;
            $scope.sumTongTienPhaiThu = data.reduce(function (sum, cur) {
            return sum + (cur.tongTienPhaiThu || 0);
          }, 0);
          $scope.sumSoTienNop = data.reduce(function (sum1, cur) {
            return sum1 + (cur.soTienNop || 0);
          }, 0);
       
        }, function () {
          _this.isLoading = false;
        });
      },
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var TMSNs = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          TMSNs.checked = false;
          this.selection.splice(idx, 1);
        } else {
          TMSNs.checked = true;
          this.selection.push(id);
        }
      },
   
      checkAll: function () {
        var _this = this;
        _this.selection = [];
        apiService.get('cashs/getByPOS', _this.tableState, function (response) {
          _this.tableData = response.data.data;
          if ($scope.checkbox == false) {
            _this.selection = [];
            $scope.checkbox = true;
          }
          else {
            for (var i = 0; i < response.data.data.length; i++) {
              var POSs = _.filter(response.data.data, function () { return response.data.data[i] })[i];
              _this.selection.push(response.data.data[i].id);
              $scope.checkbox = false;
              POSs.checked = true;
            }
          }
        }, 0);

      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.TMSNs.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      correctMultiData: function($scope) {
        var _this = $scope.TMSNs;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.CashIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cashs/correct-multi-cash-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },

      confirm: function($scope) {
        var _this = $scope.TMSNs;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.CashIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cashs/confirm-multi-cash-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },
      
      createGroupCode: function () {
        var _this = this;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var checkStatus = true;
        var existGroupCode = false;
        _.forEach(_this.selection, function(id){
          var item = _.filter(_this.tableData, function(o){return o.id === id})[0];
          if(item.transactionStatus != "S3" && item.transactionStatus != "S4"){
            checkStatus = false;
          }
          if(item.groupCode && item.groupCode !== ""){
            existGroupCode = true;
          }
        });
        var params = {};
        params.CashIds = _this.selection;
        
        if(checkStatus){
          if(existGroupCode){
            _this.checkGroupCodeExist("Thông báo");
          } else {
            apiService.create('cashs/createGroupCodeDP', { Selections: _this.selection }, function (response) {
              if (response.message == "Ok") {
                // confirmService.showTranslate('Mã nhóm mới tạo: ' + response.data.groupCode );
                _this.showDialog("Tạo group code", response.data.groupCode);
              }
            });
          }
          // apiService.create('cashs/createGroupCodeDP' , {Selections: _this.selection}, function (response) {
          //   if(response.message == "Ok"){
          //     // confirmService.showTranslate('Mã nhóm mới tạo: ' + response.data.groupCode );
          //     _this.showDialog("Tạo group code", response.data.groupCode);
          //   }
          // });
        } else {
          _this.createGroupCodeAlert("Thông báo");
        }
       
      },

      showDialog: function (titleName, groupCode) {
        // var _this = this;
        var _this = this;
        $scope.all = this;
        $ngConfirm({
          title: '<p style="font-size: 17px;">' + titleName + '</p>',
          content: 'Mã nhóm vừa tạo là: ' + '<span style="user-select: text;text-transform: uppercase;color : blue;">' + groupCode + '</span>',
          type: 'red',
          typeAnimated: true,
          scope: $scope,
          buttons: {
            close: function(scope, button){
              _this.refresh();
            },
          }
        });
      },

      createGroupCodeAlert: function(titleName) {
        var _this = this;
        $scope.all = this;
        $ngConfirm({
          title: '<p style="font-size: 17px;">' + titleName + '</p>',
          content: '<strong>Chỉ tạo group code cho trạng thái S3,S4. Vui lòng kiểm tra lại</strong>',
          type: 'red',
          typeAnimated: true,
          scope: $scope,
        });
      },

      checkGroupCodeExist: function(titleName) {
        var _this = this;
        $scope.all = this;
        $ngConfirm({
          title: '<p style="font-size: 17px;">' + titleName + '</p>',
          content: '<strong>Giao dịch đã có group code.Bạn có muốn tạo mới group code không</strong>',
          type: 'red',
          typeAnimated: true,
          scope: $scope,
          buttons: {
            close: function(scope, button){
              _this.refresh();
            },
            ok: function(scope, button){
              apiService.create('cashs/createGroupCodeDP' , {Selections: _this.selection}, function (response) {
                if(response.message == "Ok"){
                  // confirmService.showTranslate('Mã nhóm mới tạo: ' + response.data.groupCode );
                  _this.showDialog("Tạo group code", response.data.groupCode);
                }
              });
            }
          }
        });
      }

    };
  }

})();
