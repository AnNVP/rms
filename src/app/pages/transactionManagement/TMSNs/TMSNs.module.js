(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.TMSNs', {
        url: '/manage-TMSNs',
        title: 'TIEN_MAT_SAI_GON',
        templateUrl: 'app/pages/transactionManagement/TMSNs/TMSNs.html',
        controller: 'TMSNsPageCtrl',
        sidebarMeta: {
          order: 6,
        },
        'permission': 'Cashs.GetTMSN'
      });
  }

})();
