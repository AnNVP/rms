(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.paymentGateways', {
        url: '/payment-gateways',
        title: 'PAYMENT_GATEWAY',
        templateUrl: 'app/pages/transactionManagement/paymentGateways/paymentGateways.html',
        controller: 'PaymentGatewaysPageCtrl',
        sidebarMeta: {
          order: 3,
        },
        'permission': 'PaymentGateways.GetAll'
      });
  }

})();
