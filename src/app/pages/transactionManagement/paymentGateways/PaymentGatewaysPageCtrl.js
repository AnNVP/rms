(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('PaymentGatewaysPageCtrl', PaymentGatewaysPageCtrl);

  /** @ngInject */
  function PaymentGatewaysPageCtrl($scope, apiService, confirmService, notificationService, $ngConfirm) {

    $scope.paymentGates = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      tableData: [],
      selection: [],
      isSelectAll: false,
      confirmOrNot : null,
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: 'IS_NOT_NULL'
        }
      },
      transStatus:[
        {value:'',label:'Tất cả'},
        {value:'S1',label:'S1'},
        {value:'S2',label:'S2'},
        {value:'S3',label:'S3'},
        {value:'S4',label:'S4'},
        {value:'S8',label:'S8'}
      ],
      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],
      init: function (tableState) {
        $scope.paymentGates.tableState = tableState;
        $scope.paymentGates.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        _this.selection = [];
        _this.isSelectAll = false;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{orderCode:_this.default.search.orderCode}];
          searchInit.all['date'] = [
            {type:'greater', data:{timeOfPayment:_this.default.search.startDate.toDate()}},
            {type:'lesser', data:{timeOfPayment:_this.default.search.endDate.toDate()}}
          ];
        }
        _this.isInit = true;
        apiService.get('paymentgateways', _this.tableState, function (response) {
          var data = response.data.data;
          _this.tableData = data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          $scope.sumMoney = data.reduce(function (sum, cur) {
            return sum + (cur.amountMoney || 0);
          }, 0);
          _this.isLoading = false;
        }, function() {
          _this.isLoading = false;
        });
      },
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var paymentGates = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          paymentGates.checked = false;
          this.selection.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          paymentGates.checked = true;
          this.selection.push(id);
          if(this.selection.length === this.tableData.length){
            this.isSelectAll = true;
          }
        }
      },

      selectAll: function (collection) {
        var _this = this;
        if (_this.selection.length === 0) {
          angular.forEach(collection, function (val) {
            _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else if (_this.selection.length > 0 && _this.selection.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selection.indexOf(val.id);
            if (found == -1) _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else {
          _this.selection = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },
      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.paymentGates.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      confirm: function($scope) {
        var _this = $scope.paymentGates;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.paymentGatesIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('paymentgateways/confirm-mapping-multi-payment-gateway-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },

      correctMultiData: function($scope) {
        var _this = $scope.paymentGates;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.paymentGatesIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('paymentgateways/correct-multi-data-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },
    };
  }

})();
