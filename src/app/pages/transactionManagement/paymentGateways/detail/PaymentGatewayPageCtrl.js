(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('PaymentGatewayPageCtrl', PaymentGatewayPageCtrl);

  /** @ngInject */
  function PaymentGatewayPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, $ngConfirm, permissionService) {
    $scope.paymentGateway = {
      selection: [],
      confirmOrNot : null,
      isShowEditOrderCode: false,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      listTypeOfTransaction: [
        { "id": 110, "name": "KH - ĐT" },
        { "id": 120, "name": "KH - ĐT CTT" },
        { "id": 210, "name": "KH - Topica địa phương" },
        { "id": 220, "name": "KH - Topica HQ" },
        { "id": 230, "name": "KH - Topica HQ POS" },
        { "id": 240, "name": "KH-Topica HQ Tiền mặt" },
        { "id": 310, "name": "Đối tác COD - Topica HQ" },
        { "id": 320, "name": "Đối tác CTT - Topica" },
        { "id": 330, "name": "Topica SN - Topica HQ" },
        { "id": 340, "name": "POS" },
        { "id": 400, "name": "Topica - KH" },
        { "id": 500, "name": "Điều chỉnh do giảm trừ" },
        { "id": 600, "name": "Không ghi nhận doanh thu, thực thu" },
      ],
      form: {
        firstName: null,
        lastName: null,
        email: null,
        cardNumber: null,
        amountMoney: null,
        paymentContent: null,
        timeOfPayment: null,
        transactionStatus: null,
        orderCode: null,
        ssrsTransactionStatus: null,
      },
      back: function () {
        $state.go('transactionManagement.paymentGateways');
      },
      reload: function () {
        $state.reload();
      },
      loadDetail: function (data) {
        var _this = this;
        apiService.get('orders/' + data.id, null, function (response) {
          _this.form.orderDetails = response.data.orderDetails;
          _this.form.order = response.data;
          if(data.orderCode != _this.form.orderCode){
            _.forEach(_this.form.orderDetails, function (element) {
              element.soTienNopHienTai = (element.giaBanSauKhuyenMai / response.data.tongTienPhaiThu) * _this.form.amountMoney;
              element.soTienNopHienTaiCu = 0;
              element.soTru = element.soTienNopHienTai;
            })
          }
          else{
            _.forEach(_this.form.orderDetails, function(element){
              element.soTienNopHienTaiCu = element.soTienNopHienTai;
              element.soTru = 0;
            })
          }
          // _this.form.orderCode = response.data.orderCode;
        });
      },
      init: function () {
        var _this = this;
        // _this.initRoutes();
        // if (!_this.isCreateMode) {
        apiService.get('paymentgateways/' + $stateParams.paymentGatewayId, null, function (response) {
          _this.form = response.data;
          _this.isShowEditOrderCode = permissionService.isAllowed(['TVTS','PM_TVTS','Advisor','ADMIN']);
          if(_this.form.ssrsTransactionStatus == _this.transactionStatus.S1 || _this.form.ssrsTransactionStatus == _this.transactionStatus.S3)
          apiService.get('Orders/get-order-for-transaction-management', null, function (response) {
            _this.listOrder = response.data;
            if(_this.form.orderCode != null){
              _this.form.donHang = _.filter(_this.listOrder, function (element) {
                return element.orderCode == _this.form.orderCode
              })[0];
            }
            _this.isLoading = false;
          });
          if (_this.form.orderCode != null) {
            var data = {};
            data.orderCode = _this.form.orderCode;
            data.id = response.data.id;
            apiService.get('Orders/get-by-order-code-for-payment-gateway', data, function (response) {
              _this.form.orderDetails = response.data.orderDetails;
              _this.form.order = response.data;
              _.forEach(_this.form.orderDetails, function(element){
                element.soTienNopHienTaiCu = element.soTienNopHienTai;
                element.soTru = 0;
              })
              // if (_this.form.ssrsTransactionStatus != _this.transactionStatus.S4) {
              //   _.forEach(_this.form.orderDetails, function (element) {
              //     element.soTienNopHienTai = (element.giaBanSauKhuyenMai / response.data.tongTienPhaiThu) * _this.form.amountMoney
              //   })
              // }
            });
          }
        });
      },
      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.paymentGateway.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      confirm: function ($scope) {
        var _this = $scope.paymentGateway;
        var data = {};
        data.id = $stateParams.paymentGatewayId;
        data.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('paymentgateways/confirm-mapping-single-payment-gateway-transaction-manage', data, function (response) {
          _this.reload(); 
        });
      },
      correctSingleData: function ($scope) {
        var _this = $scope.paymentGateway;
        var data = {};
        data.id = $stateParams.paymentGatewayId;
        data.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('paymentgateways/correct-single-data-transaction-manage', data, function (response) {
          _this.reload();
        });
      },
      save: function () {
        var _this = this;
        var data = {};
        data.id = $stateParams.paymentGatewayId;
        data.orderCode = _this.form.orderDetails[0].orderCode;
        data.orderDetails = _this.form.orderDetails;
        data.ssrsTransactionStatus = _this.form.transactionStatus;
        data.typeOfTransaction = _this.form.typeOfTransaction;
        data.typeOfPayment = 3;

        apiService.update('paymentGateways', data, function (response) {
          _this.reload();
        })
      }
    };
  }

})();
