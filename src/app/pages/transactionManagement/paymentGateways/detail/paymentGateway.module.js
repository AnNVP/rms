(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.paymentGatewayDetail', {
        url: '/payment-gateways/{paymentGatewayId}',
        title: 'PAYMENT_GATEWAY',
        templateUrl: 'app/pages/transactionManagement/paymentGateways/detail/paymentGateway.html',
        controller: 'PaymentGatewayPageCtrl',
        'permission': 'PaymentGateways.GetById'
      });
  }

})();
