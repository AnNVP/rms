(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('CodsPageCtrl', CodsPageCtrl);

  /** @ngInject */
  function CodsPageCtrl($scope, apiService, confirmService, notificationService,$ngConfirm, permissionService) {

    $scope.cods = {
      isLoading: true,
      tableState: null,
      tablePageSize: 50,
      tableData: [],
      selection: [],
      role: null,
      isSelectAll: false,
      confirmOrNot : null,
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: 'IS_NOT_NULL'
        }
      },
      partners:[
        {value:'', label:'Tất cả'},
        {value:'GHTK',label:'GHTK'},
        {value:'GHOV',label:'GHOV'},
        {value:'OVG',label:'OVG'},
      ],
      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],
      init: function (tableState) {
        $scope.cods.tableState = tableState;
        $scope.cods.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        _this.selection = [];
        _this.isSelectAll = false;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{orderCode:_this.default.search.orderCode}];
          searchInit.all['date'] = [
            {type:'greater', data:{timeOfPayment:_this.default.search.startDate.toDate()}},
            {type:'lesser', data:{timeOfPayment:_this.default.search.endDate.toDate()}}
          ];
        }
        _this.isInit = true;
        apiService.get('users/get-role-by-email', null, function (response) {
          _this.role = response.data;
        });
        apiService.get('cods', searchInit, function (response) {
          var data = response.data.data;
          _this.tableData = data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          $scope.sumMoney = data.reduce(function (sum, cur) {
            return sum + (cur.actualReceivedMoney || 0);
          }, 0);
          _this.isLoading = false;
        }, function() {
          _this.isLoading = false;
        });
      },
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var cod = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          cod.checked = false;
          this.selection.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          cod.checked = true;
          this.selection.push(id);
          if(this.selection.length === this.tableData.length){
            this.isSelectAll = true;
          }
        }
      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.cods.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      selectAll: function (collection) {
        var _this = this;
        if (_this.selection.length === 0) {
          angular.forEach(collection, function (val) {
            _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else if (_this.selection.length > 0 && _this.selection.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selection.indexOf(val.id);
            if (found == -1) _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else {
          _this.selection = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },

//dùng chung cho 2 hàm Đúng dữ liệu : 1 và hủy dữ liệu: 0
      confirm: function($scope) {
        var _this = $scope.cods;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.codsIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cods/confirm-mapping-multi-cod-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },
      correctMultiData: function($scope) {
        var _this = $scope.cods;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {};
        params.codsIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cods/correct-multi-data-transaction-management', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },
      // createGroupCode: function() {
      //   var _this = this;

      //   if (_this.selection.length === 0) {
      //     notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
      //     return false;
      //   }

      //   var params = {};
      //   params.codsIds = _this.selection;

      //   apiService.update('cods/create-group-code', params, function (response) {
      //     _this.selection = [];
      //     _this.refresh();
      //   });
      // },
    };
  }

})();
