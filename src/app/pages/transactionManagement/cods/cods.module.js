(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.cods', {
        url: '/cods',
        title: 'CODS',
        templateUrl: 'app/pages/transactionManagement/cods/cods.html',
        controller: 'CodsPageCtrl',
        sidebarMeta: {
          order: 2,
        },
        'permission': 'Cods.GetAll'
      });
  }

})();
