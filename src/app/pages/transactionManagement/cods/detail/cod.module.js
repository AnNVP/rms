(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.codDetail', {
        url: '/cods/{codId}',
        title: 'CODS',
        templateUrl: 'app/pages/transactionManagement/cods/detail/cod.html',
        controller: 'CodPageCtrl',
        'permission': 'Cods.GetById'
      });
  }

})();
