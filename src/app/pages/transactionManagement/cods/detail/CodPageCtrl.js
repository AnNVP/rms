(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('CodPageCtrl', CodPageCtrl);

  /** @ngInject */
  function CodPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate , $ngConfirm, permissionService) {
    $scope.cod = {
      selection: [],
      confirmOrNot : null,
      orderCodeEdit: false,
      isShowEditOrderCode: false,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      listTypeOfTransaction: [
        { "id": 110, "name": "KH - ĐT" },
        { "id": 120, "name": "KH - ĐT CTT" },
        { "id": 210, "name": "KH - Topica địa phương" },
        { "id": 220, "name": "KH - Topica HQ" },
        { "id": 230, "name": "KH - Topica HQ POS" },
        { "id": 240, "name": "KH-Topica HQ Tiền mặt" },
        { "id": 310, "name": "Đối tác COD - Topica HQ" },
        { "id": 320, "name": "Đối tác CTT - Topica" },
        { "id": 330, "name": "Topica SN - Topica HQ" },
        { "id": 340, "name": "POS" },
        { "id": 400, "name": "Topica - KH" },
        { "id": 500, "name": "Điều chỉnh do giảm trừ" },
        { "id": 600, "name": "Không ghi nhận doanh thu, thực thu" },
      ],
      form: {
        kaitoBillOfLading: null,
        partnerBillOfLading: null,
        nameOfCod: null,
        timeOfPayment: null,
        actualReceivedMoney: null,
        orderCode: null,
        transactionStatus: null,
        groupCode: null,
      },
      back: function () {
        $state.go('transactionManagement.cods');
      },
      reload: function () {
        $state.reload();
      },
      loadDetail: function (data) {
        var _this = this;
        apiService.get('orders/' + data.id, null, function (response) {
          _this.form.orderDetails = response.data.orderDetails;
          if(data.orderCode != _this.form.orderCode){
            _.forEach(_this.form.orderDetails, function (element) {
              element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.actualReceivedMoney;
              element.soTienNopHienTaiCu = 0;
              element.soTru = element.soTienNopHienTai;
            })
          }
          else{
            _.forEach(_this.form.orderDetails, function (element) {
              element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.actualReceivedMoney;
              element.soTienNopHienTaiCu = element.soTienNopHienTai;
              element.soTru = 0;
            })
          }
          // _this.form.orderCode = response.data.orderCode;
        });
      },
      init: function () {
        var _this = this;
        apiService.get('cods/' + $stateParams.codId, null, function (response) {
          _this.form = response.data;
          _this.orderCodeEdit = false;
          switch (_this.form.transactionStatus) {
            case _this.transactionStatus.S1:
              _this.orderCodeEdit = permissionService.isAllowed(['PM_TVTS','TVTS','Advisor','ADMIN']);
              break;
            case _this.transactionStatus.S2:
              _this.orderCodeEdit = permissionService.isAllowed(['PM_TVTS','TVTS','Advisor','ADMIN']);
              break;
            case _this.transactionStatus.S3:
              _this.orderCodeEdit = permissionService.isAllowed(['LeaderKTKH','KTKH_HN','ADMIN','TVTS','PM_TVTS','Advisor']);
              break;
            case _this.transactionStatus.S8:
              _this.orderCodeEdit = permissionService.isAllowed(['LeaderKTKH']);
              break;
          }
          _this.isShowEditOrderCode = permissionService.isAllowed(['TVTS','QTTT','PM_QTTT','PM_TVTS','Advisor','ADMIN']);
          if(_this.form.transactionStatus == _this.transactionStatus.S1 || _this.form.transactionStatus == _this.transactionStatus.S3)
          apiService.get('Orders/get-order-for-transaction-management', null, function (response) {
            _this.listOrder = response.data;
            if(_this.form.orderCode != null){
              _this.form.donHang = _.filter(_this.listOrder, function (element) {
                return element.orderCode == _this.form.orderCode;
              })[0];
            }
            _this.isLoading = false;
          });
          if (_this.form.orderCode != null) {
            var data = {};
            data.orderCode = _this.form.orderCode;
            data.id = response.data.id;
            apiService.get('Orders/get-by-order-code-for-cod', data, function (response) {
              _this.form.orderDetails = response.data.orderDetails;
              _this.form.order = response.data;
              // if (_this.form.transactionStatus != _this.transactionStatus.S4) {
                _.forEach(_this.form.orderDetails, function (element) {
                  element.soTienNopHienTaiCu = element.soTienNopHienTai;
                  element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.actualReceivedMoney;
                  element.soTru = 0;
                })
              // }
            });
          }
        });
        // }
      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.cod.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      confirm: function ($scope) {
        var _this = $scope.cod;
        var data = {};
        data.id = $stateParams.codId;
        data.confirmOrUnconfirm = _this.confirmOrNot;
        apiService.update('cods/confirm-mapping-single-cod-transaction-manage', data, function (response) {
          _this.reload();
        });
      },

      correctSingleData: function ($scope) {
        var _this = $scope.cod;
        var data = {};
        data.id = $stateParams.codId;
        data.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('cods/correct-single-data-transaction-management', data, function (response) {
          _this.reload();
        });
      },

      save: function () {
        var _this = this;
        var data = {};
        data.id = $stateParams.codId;
        data.orderCode = _this.form.orderDetails[0].orderCode;
        data.orderDetails = _this.form.orderDetails;
        data.ssrsTransactionStatus = _this.form.transactionStatus;
        data.typeOfTransaction = _this.form.typeOfTransaction;
        data.typeOfPayment = 4;

        apiService.update('cods', data, function (response) {
          _this.reload();
        })
      }

    };
  }

})();
