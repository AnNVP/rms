(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('AllSmssPageCtrl', AllSmssPageCtrl);

  /** @ngInject */
  function AllSmssPageCtrl($scope, apiService, confirmService, notificationService) {
    $scope.allSmss = {
      isLoading: true,
      tableState: null,
      tablePageSize: 50,
      tableData: [],
      selection: [],
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: 'IS_NOT_NULL'
        }
      },
      transactionStatus: {
        S1: 'S1        ',
        S2: 'S2        ',
        S3: 'S3        ',
        S8: 'S8        ',
      },
      banks:[
        { value: '', label: 'Tất cả' },
        {value:'Techcombank',label:'Techcombank'},
        {value:'VietinBank',label:'VietinBank'},
        {value:'BIDV',label:'BIDV'},
        {value:'Vietcombank',label:'Vietcombank'},
        {value:'ACB',label:'ACB'},
        {value:'Sacombank',label:'Sacombank'},
        {value:'AGRIBANK',label:'AGRIBANK'},
        {value:'VPBank',label:'VPBank'}
      ],
     
      bankAccounts:[
        { value: '', label: 'Tất cả' },
        {value:'12210000687553', label: '12210000687553'},
        {value:'1500201089256', label: '1500201089256'},
        {value:'1303201050134', label: '1303201050134'},
        {value:'203383399', label: '203383399'},
        {value:'20037363333', label: '20037363333'},
        {value:'19128170636012', label: '19128170636012'},
        {value:'0011004164283', label: '0011004164283'},
        {value:'301000006998', label: '301000006998'},
        {value:'115000130666', label: '115000130666'},
        {value:'113000169711', label: '113000169711'},
        {value:'61093206', label: '61093206'},
        {value:'112596593', label: '112596593'},
      ],

      bankLabel: {
        'Techcombank':'TECH',
        'VietinBank':'VIETIN',
        'BIDV':'BIDV',
        'Vietcombank':'VIETCOM',
        'ACB':'ACB',
        'Sacombank': 'SACOM',
        'AGRIBANK':'AGR',
        'VPBank':'VP'
      },

      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],

      init: function (tableState) {
        $scope.allSmss.tableState = tableState;
        $scope.allSmss.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{orderCode:_this.default.search.orderCode}];
          searchInit.all['date'] = [
            {type:'greater', data:{sentTime:_this.default.search.startDate.toDate()}},
            {type:'lesser', data:{sentTime:_this.default.search.endDate.toDate()}}
          ];
        }
        _this.isInit = true;
        apiService.get('bankingsmss', searchInit, function (response) {
          _this.tableData = response.data.data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function() {
          _this.isLoading = false;
        });
      },

      bankChange: function(value) {
        var _this = this;
        apiService.get('smss/accounts-by-bank/' + value, null, function(response){
          var account = response.data || [];
          if(account.length > 0){
            _this.bankAccounts = [];
            if(account.length > 1){
              _this.bankAccounts.push({ value: '', label: 'Tất cả' });
            }
            account.forEach(function(item, index){
              _this.bankAccounts.push({value: item, label: item});
            })
          }
        })
      }
    };
  }

})();
