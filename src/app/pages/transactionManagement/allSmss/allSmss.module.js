(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.allSmss', {
        url: '/all-smss',
        title: 'BANKING_SMS',
        templateUrl: 'app/pages/transactionManagement/allSmss/allSmss.html',
        controller: 'AllSmssPageCtrl',
        sidebarMeta: {
          order: 0,
        },
        'permission': 'BankingSmss.GetAll'
      });
  }

})();
