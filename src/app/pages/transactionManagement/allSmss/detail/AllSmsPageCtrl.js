(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('AllSmsPageCtrl', AllSmsPageCtrl);

  /** @ngInject */
  function AllSmsPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate) {
    $scope.allSms = {
      selection: [],
      transactionStatus: {
        S1: 'S1        ',
        S2: 'S2        ',
        S3: 'S3        ',
        S8: 'S8        ',
      },
      listOrder: [],
      form: {
        bank: null,
        sentTime: null,
        contentSms: null,
        bankAccountNo: null,
        sentMoney: null,
        balance: null,
        sentReason: null,
        orderCode: null,
        paymentType: null,
        transactionStatus: null,
        typeOfTransaction: null,
        bankCode: null,
        extractedSmsId: null,
      },
      listTrangThaiOrder: [
        { "id": "O1", "name": "ORDER_CHUA_MAP" },
        { "id": "O2", "name": "HOAN_THANH_70_KHOAN_THU" },
        { "id": "O3", "name": "HOAN_THANH_TREN_70_KHOAN_THU" },
        { "id": "O4", "name": "HOAN_THANH_100_KHOAN_THU" },
        { "id": "O5", "name": "ORDER_BI_HUY" },
      ],
      listTypeOfTransaction: [
        { "id": 110, "name": "KH - ĐT" },
        { "id": 120, "name": "KH - ĐT CTT" },
        { "id": 210, "name": "KH - Topica địa phương" },
        { "id": 220, "name": "KH - Topica HQ" },
        { "id": 230, "name": "KH - Topica HQ POS" },
        { "id": 240, "name": "KH-Topica HQ Tiền mặt" },
        { "id": 310, "name": "Đối tác COD - Topica HQ" },
        { "id": 320, "name": "Đối tác CTT - Topica" },
        { "id": 330, "name": "Topica SN - Topica HQ" },
        { "id": 340, "name": "POS" },
        { "id": 400, "name": "Topica - KH" },
        { "id": 500, "name": "Điều chỉnh do giảm trừ" },
        { "id": 600, "name": "Không ghi nhận doanh thu, thực thu" },
      ],
      back: function () {
        $state.go('transactionManagement.allSmss');
      },
      reload: function () {
        $state.reload();
      },
      // loadDetail: function (data) {
      //   var _this = this;
      //   apiService.get('orders/' + data.id, null, function (response) {
      //     _this.form.orderDetails = response.data.orderDetails;
      //     _this.form.order = response.data;
      //     _this.form.order.orderStatusObject = _.filter(_this.listTrangThaiOrder, function(element)
      //     {return element.id == response.data.orderStatus})[0];
      //     console.log(_this.form.orderStatusObject);
      //     if(_this.form.transactionStatus != _this.transactionStatus.S4){
      //       _.forEach(_this.form.orderDetails, function(element){
      //         element.soTienNopHienTai = (element.giaBanSauKhuyenMai/response.data.tongTienPhaiThu) * _this.form.sentMoney
      //       })
      //     }
      //     //_this.form.orderCode = response.data.orderCode;
      //   });
      // },
      init: function() {
        var _this = this;
        // _this.initRoutes();
        // if (!_this.isCreateMode) {
          apiService.get('bankingsmss/' + $stateParams.allSmssId, null, function (response) {
            _this.form = response.data;
            // if(_this.form.orderCode == null){
            //   apiService.get('Orders/get-order-for-transaction-management', null, function (response) {
            //     _this.listOrder = response.data;
            //     _this.isLoading = false;
            //   });
            // }
            // else{
              var data = {};
              data.orderCode = _this.form.orderCode;
              data.id = response.data.id;
              apiService.get('Orders/get-by-order-code-for-bank-transfer', data, function (response) {
                _this.form.orderDetails = response.data.orderDetails;
                _this.form.order = response.data;
                if(_this.form.transactionStatus != _this.transactionStatus.S3){
                  _.forEach(_this.form.orderDetails, function(element){
                    element.soTienNopHienTai = (element.giaBanSauKhuyenMai/response.data.tongTienPhaiThu) * _this.form.sentMoney
                  })
                }
              });
            // }
            // _this.selection = _.map(response.data.bankTransferRoutes, function (o) {
            //   return o.route.id;
            // });
          });
        // }       
      },
    };
  }

})();
