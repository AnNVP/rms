(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.allSmsDetail', {
        url: '/all-smss/{allSmssId}',
        title: 'BANKING_SMS',
        templateUrl: 'app/pages/transactionManagement/allSmss/detail/allSms.html',
        controller: 'AllSmsPageCtrl',
        'permission': 'BankingSmss.GetById'
      });
  }

})();
