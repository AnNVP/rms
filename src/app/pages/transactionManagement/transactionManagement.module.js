(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.transactionManagement', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('transactionManagement', {
          url: '/transaction-management',
          template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
          abstract: true,
          title: 'TRANSACTION_MANAGEMENT',
          sidebarMeta: {
            icon: 'ion-settings',
            order: 1,
          },
          'permission': ['Smss.GetAll','PaymentGateways.GetAll','Cods.GetAll','PaymentGateways.GetAll',
          'Cashs.GetByPOS','Cashs.GetTMHN','Cashs.GetTMSN']
        });
    }
  
  })();
  