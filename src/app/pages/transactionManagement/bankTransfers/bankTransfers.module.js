(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.bankTransfers', {
        url: '/bank-transfers',
        title: 'BANK_TRANSFER',
        templateUrl: 'app/pages/transactionManagement/bankTransfers/bankTransfers.html',
        controller: 'BankTransfersPageCtrl',
        sidebarMeta: {
          order: 1,
        },
        'permission': 'Smss.GetAll'
      });
  }

})();
