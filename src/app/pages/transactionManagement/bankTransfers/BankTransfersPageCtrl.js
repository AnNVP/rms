(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('BankTransfersPageCtrl', BankTransfersPageCtrl);

  /** @ngInject */
  function BankTransfersPageCtrl($scope, apiService, confirmService, notificationService, $ngConfirm) {
    $scope.startDate = moment().startOf('month');
    $scope.endDate = moment().endOf('month');
    $scope.bankTransfers = {
      isLoading: true,
      tableState: null,
      tablePageSize: 50,
      tableData: [],
      selection: [],
      isSelectAll: false,
      confirmOrNot : null,
      isInit: false,
      default: {
        search: {
          startDate: moment().startOf('month'),
          endDate: moment().endOf('month'),
          orderCode: 'IS_NOT_NULL',
          bank: '',
          bankAccount: ''
        }
      },

      banks: [
        { value: '', label: 'Tất cả' },
        { value: 'Techcombank', label: 'Techcombank' },
        { value: 'VietinBank', label: 'VietinBank' },
        { value: 'BIDV', label: 'BIDV' },
        { value: 'Vietcombank', label: 'Vietcombank' },
        { value: 'ACB', label: 'ACB' },
        { value: 'Sacombank', label: 'Sacombank' },
        { value: 'AGRIBANK', label: 'AGRIBANK' },
        { value: 'VPBank', label: 'VPBank' }
      ],
      bankAccounts: [
        { value: '', label: 'Tất cả' },
        { value: '12210000687553', label: '12210000687553' },
        { value: '1500201089256', label: '1500201089256' },
        { value: '1303201050134', label: '1303201050134' },
        { value: '203383399', label: '203383399' },
        { value: '20037363333', label: '20037363333' },
        { value: '19128170636012', label: '19128170636012' },
        { value: '0011004164283', label: '0011004164283' },
        { value: '301000006998', label: '301000006998' },
        { value: '115000130666', label: '115000130666' },
        { value: '113000169711', label: '113000169711' },
        { value: '61093206', label: '61093206' },
        { value: '112596593', label: '112596593' },
      ],
      transactionStatus: [
        { value: '', label: 'Tất cả' },
        { value: 'S1', label: 'S1' },
        { value: 'S2', label: 'S2' },
        { value: 'S3', label: 'S3' },
        { value: 'S4', label: 'S4' },
        { value: 'S8', label: 'S8' },
      ],
      bankLabel: {
        'Techcombank':'TECH',
        'VietinBank':'VIETIN',
        'BIDV':'BIDV',
        'Vietcombank':'VIETCOM',
        'ACB':'ACB',
        'Sacombank': 'SACOM',
        'AGRIBANK':'AGR',
        'VPBank':'VP'
      },
      myTransaction: [
        { value: '', label: 'Tất cả' },
        { value: 'IS_NOT_NULL', label: 'Của tôi' },
        { value: 'IS_NULL', label: 'Chưa xác định' }
      ],
      init: function (tableState) {
        $scope.bankTransfers.tableState = tableState;
        $scope.bankTransfers.refresh();
      },
      refresh: function () {
        var _this = this;
        _this.isLoading = true;
        _this.selection = [];
        _this.isSelectAll = false;
        var searchInit = _this.tableState;
        if (!_this.isInit) {
          searchInit.all = {};
          searchInit.all['checkbox'] = [{orderCode:_this.default.search.orderCode}];
          searchInit.all['date'] = [
            {type:'greater', data:{sentTime:_this.default.search.startDate.toDate()}},
            {type:'lesser', data:{sentTime:_this.default.search.endDate.toDate()}}
          ];
        }
        _this.isInit = true;
        apiService.get('smss', searchInit, function (response) {
          var data = response.data.data;
          _this.tableData = data;
          _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          $scope.sumSentMoney = data.reduce(function (sum, cur) {
            return sum + (cur.sentMoney || 0);
          }, 0);
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var bankTransfer = _.filter(this.tableData, function (o) { return o.id === id; })[0];
        if (idx > -1) {
          bankTransfer.checked = false;
          this.selection.splice(idx, 1);
          this.isSelectAll = false;
        } else {
          bankTransfer.checked = true;
          this.selection.push(id);
          if (this.selection.length === this.tableData.length) {
            this.isSelectAll = true;
          }
        }
      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.bankTransfers.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      selectAll: function (collection) {
        var _this = this;
        if (_this.selection.length === 0) {
          angular.forEach(collection, function (val) {
            _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else if (_this.selection.length > 0 && _this.selection.length != _this.tableData.length) {
          angular.forEach(collection, function (val) {
            var found = _this.selection.indexOf(val.id);
            if (found == -1) _this.selection.push(val.id);
            val.checked = true;
          });
          _this.isSelectAll = true;
        } else {
          _this.selection = [];
          angular.forEach(collection, function (val) {
            val.checked = false;
          });
          _this.isSelectAll = false;
        }
      },

      //dùng chung cho 2 hàm Đúng dữ liệu : 1 và hủy dữ liệu: 0
      confirm: function ($scope) {
        var _this = $scope.bankTransfers;

        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        var params = {};
        params.bankTransfersIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('smss/confirm-mapping-multi-bank-transfer-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },

      //dùng chung cho 2 hàm Đúng dữ liệu : 1 và hủy dữ liệu: 0
      correctMultiData: function ($scope) {
        var _this = $scope.bankTransfers;

        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        var params = {};
        params.bankTransfersIds = _this.selection;
        params.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('smss/correct-multi-bank-transfer-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },

      lockMultiData: function () {
        var _this = this;

        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }

        var params = {};
        params.bankTransfersIds = _this.selection;

        apiService.update('smss/lock-multi-bank-transfer-transaction-manage', params, function (response) {
          _this.selection = [];
          _this.refresh();
        });
      },

      bankChange: function (value) {
        var _this = this;
        apiService.get('smss/accounts-by-bank/' + value, null, function (response) {
          var account = response.data || [];
          if (account.length > 0) {
            _this.bankAccounts = [];
            if(account.length > 1){
              _this.bankAccounts.push({ value: '', label: 'Tất cả' });
            }
            account.forEach(function (item, index) {
              _this.bankAccounts.push({ value: item, label: item });
            })
          }
        })
      },
    };
  }

})();
