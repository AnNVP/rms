(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('transactionManagement.bankTransferDetail', {
        url: '/bank-transfers/{bankTransferId}',
        title: 'BANK_TRANSFER',
        templateUrl: 'app/pages/transactionManagement/bankTransfers/detail/bankTransfer.html',
        controller: 'BankTransferPageCtrl',
        'permission': 'Smss.GetById'
      });
  }

})();
