(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transactionManagement')
    .controller('BankTransferPageCtrl', BankTransferPageCtrl);

  /** @ngInject */
  function BankTransferPageCtrl($scope, $timeout, $state, $stateParams, apiService, $translate, permissionService, $ngConfirm) {
    $scope.bankTransfer = {
      selection: [],
      confirmOrNot : null,
      transactionStatus: {
        S1: 'S1',
        S2: 'S2',
        S3: 'S3',
        S4: 'S4',
        S8: 'S8',
      },
      isHide: false,
      orderCodeEdit: false,
      typeOfTransactionEdit: false,
      isHideConfirmMapping: false,
      isEdit: false,
      listOrder: [],
      form: {
        bank: null,
        sentTime: null,
        contentSms: null,
        bankAccountNo: null,
        sentMoney: null,
        balance: null,
        sentReason: null,
        orderCode: null,
        paymentType: null,
        transactionStatus: null,
        typeOfTransaction: null,
        bankCode: null
      },
      listTrangThaiOrder: [
        { "id": "O1", "name": "ORDER_CHUA_MAP" },
        { "id": "O2", "name": "HOAN_THANH_70_KHOAN_THU" },
        { "id": "O3", "name": "HOAN_THANH_TREN_70_KHOAN_THU" },
        { "id": "O4", "name": "HOAN_THANH_100_KHOAN_THU" },
        { "id": "O5", "name": "ORDER_BI_HUY" },
      ],
      listTypeOfTransaction: [
        // { "id": 110, "name": "110_KH_DT" },
        // { "id": 120, "name": "120_KH_DT_CTT" },
        // { "id": 210, "name": "210_KH_Topica_DP" },
        { "id": 220, "name": "220_KH_Topica_HQ" },
        // { "id": 230, "name": "230_KH_Topica_HQ_POS" },
        // { "id": 240, "name": "240_KH_Topica_HQ_TM" },
        { "id": 310, "name": "310_COD_Topica_HQ" },
        { "id": 320, "name": "320_CTT_Topica" },
        { "id": 330, "name": "330_Topica_SN_Topica_HQ" },
        { "id": 340, "name": "340_POS" },
        // { "id": 400, "name": "400_Topica_KH" },
        { "id": 500, "name": "500_DIEU_CHINH_GIAM_TRU" },
        { "id": 600, "name": "600_KHONG_GHI_NHAN" },
      ],
      back: function () {
        $state.go('transactionManagement.bankTransfers');
      },
      reload: function () {
        $state.reload();
      },
      loadDetail: function (data) {
        var _this = this;
        apiService.get('orders/' + data.id, null, function (response) {
          _this.form.orderDetails = response.data.orderDetails;
          _this.form.order = response.data;
          _this.form.order.orderStatusObject = _.filter(_this.listTrangThaiOrder, function (element) { return element.id == response.data.orderStatus })[0];
          if (_this.form.transactionStatus != _this.transactionStatus.S4) {
           if(data.orderCode != _this.form.orderCode){
            _.forEach(_this.form.orderDetails, function (element) {
              element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.sentMoney;
              element.soTienNopHienTaiCu = 0;
              element.soTru = element.soTienNopHienTai;
            })
           }
           else{
            _.forEach(_this.form.orderDetails, function (element) {
              element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.sentMoney;
              element.soTienNopHienTaiCu = element.soTienNopHienTai;
              element.soTru = 0;
            })
           }
          }
          //_this.form.orderCode = response.data.orderCode;
        });
      },
      init: function () {
        var _this = this;
        // _this.initRoutes();
        // if (!_this.isCreateMode) {
        apiService.get('smss/' + $stateParams.bankTransferId, null, function (response) {
          _this.form = response.data;
          _this.showHideDetail(response.data.typeOfTransaction);
          // _this.orderCodeEdit = false;
          _this.typeOfTransactionEdit = !permissionService.isAllowed(['TVTS','ADMIN','PM_KTKH','KTKH_HN']);
          _this.isHideConfirmMapping = permissionService.isAllowed(['TVTS','PM_TVTS','Advisor','KTKH_HN','PM_KTKH','ADMIN']);
          // _this.isHideConfirmMapping = permissionService.isAllowed(['TVTS','PM_TVTS','Advisor','KTKH_HN','PM_KTKH','ADMIN']);
        //  switch (_this.form.transactionStatus) {
        //     case _this.transactionStatus.S1:
        //       _this.orderCodeEdit = permissionService.isAllowed(['PM_TVTS','TVTS','Advisor','ADMIN']);
        //       break;
        //     case _this.transactionStatus.S2:
        //       _this.orderCodeEdit = permissionService.isAllowed(['PM_TVTS','TVTS','Advisor','ADMIN']);
        //       break;
        //     case _this.transactionStatus.S3:
        //       _this.orderCodeEdit = permissionService.isAllowed(['LeaderKTKH','KTKH_HN','ADMIN','TVTS','PM_TVTS','Advisor']);
        //       break ;
        //     case _this.transactionStatus.S8:
        //       _this.orderCodeEdit = permissionService.isAllowed(['LeaderKTKH']);
        //       break;
        //   }
          _this.isEdit = _this.orderCodeEdit;
          if(_this.form.transactionStatus == _this.transactionStatus.S1 || _this.form.transactionStatus == _this.transactionStatus.S3)
          apiService.get('Orders/get-order-for-transaction-management', null, function (response) {
            _this.listOrder = response.data;
            if (_this.form.orderCode != null) {
              _this.form.donHang = _.filter(_this.listOrder, function (element) {
                return element.orderCode == _this.form.orderCode
              })[0];
            }
            _this.isLoading = false;
          });
          if (_this.form.orderCode != null) {
            var data = {};
            data.orderCode = _this.form.orderCode;
            data.id = response.data.id;
            apiService.get('Orders/get-by-order-code-for-bank-transfer', data, function (response) {
              _this.form.orderDetails = response.data.orderDetails;
              console.log(response.data);
              _this.form.order = response.data;
              _this.form.order.orderStatusObject = _.filter(_this.listTrangThaiOrder, function (element) 
              { return element.id == response.data.orderStatus })[0];
              //if (_this.form.transactionStatus != _this.transactionStatus.S3) {
                _.forEach(_this.form.orderDetails, function (element) {
                  element.soTienNopHienTaiCu = element.soTienNopHienTai;
                  element.soTienNopHienTai = (element.soTienPhaiNop / response.data.tongTienPhaiThu) * _this.form.sentMoney;
                  element.soTru = 0;
                })
              //}
            });
          }
        });
      },

      //hàm này dùng cho cả hủy và xác nhận
      confirmDialog: function (titleName, callback, confirmOrNot) {
        $scope.bankTransfer.confirmOrNot = confirmOrNot;
        if (confirmOrNot == 0) {
          $ngConfirm({
            title: '<p style="font-size: 15px; !important">' + titleName + '</p>',
            content: 'Bạn có chắc chắn muốn hủy không. Tất cả các giao dịch về bị rollback lại từ đầu.',
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            scope: $scope,
            buttons: {
              accept: {
                text: 'Đông ý',
                btnClass: 'btn-blue',
                action: function (scope, button) {
                  callback(scope);
                }
              },
              cancel: {
                text: 'Từ Chối',
                btnClass: 'btn-red',
                action: function (scope, button) {
                  return true;
                }
              },
            }
          });
        }
        else {
          callback($scope);
        }
      },

      confirm: function ($scope) {
        var _this = $scope.bankTransfer;
        var data = {};
        data.id = $stateParams.bankTransferId;
        data.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('smss/confirm-mapping-single-bank-transfer-transaction-manage', data, function (response) {
          _this.reload();
        });
      },

      correctSingleData: function ($scope) {
        var _this = $scope.bankTransfer;
        var data = {};
        data.id = $stateParams.bankTransferId;
        data.confirmOrUnconfirm = _this.confirmOrNot;

        apiService.update('smss/correct-single-bank-transfer-transaction-manage', data, function (response) {

          _this.reload();
        });
      },

      lockSingleData: function () {
        var _this = this;
        var data = {};
        data.id = $stateParams.bankTransferId;

        apiService.update('smss/lock-single-bank-transfer-transaction-manage', data, function (response) {
          _this.reload();
        });
      },

      showHideDetail: function(data){
        var _this = this;
        if(data == 340 || data == 320 || data == 500 || data == 600){
          _this.isHide = true;
        }
        else{
          _this.isHide = false;
        }
      },

      save: function () {
        var _this = this;
        var data = {};
        data.TypeOfTransaction = _this.form.typeOfTransaction;
        data.Id = $stateParams.bankTransferId;
        if (data.TypeOfTransaction != 320 && data.TypeOfTransaction != 340 &&
           data.TypeOfTransaction != 310 && data.TypeOfTransaction != 330 &&
           data.TypeOfTransaction != 500 && data.TypeOfTransaction != 600) {
          data.TypeOfPayment = 1;
          //data.orderCode = _this.form.orderDetails[0].orderCode;
          if(_this.form.donHang == null ){
            data.OrderCode = _this.form.orderCode;
          }
          else{
            data.OrderCode = _this.form.donHang.orderCode;
          }
          data.OrderDetails = _this.form.orderDetails;
          data.SsrsTransactionStatus = _this.form.transactionStatus;
          apiService.update('smss', data, function (response) {
            _this.reload();
          })
        }
        else {
          if(data.TypeOfTransaction == 310 || data.TypeOfTransaction == 330){
            if(_this.form.donHang == null){
              data.OrderCode = _this.form.orderCode;
            }
            else{
              data.OrderCode = _this.form.donHang.orderCode;
            }
          }
          apiService.update('smss/update-for-pos-and-paygate', data, function (response) {
            _this.reload();
          })
        }
      }
    };
  }

})();
