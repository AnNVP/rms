(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_NTL')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_NTL.ThucThu', {
        url: '/ThucThu',
        title: 'Thực thu NTL',
        templateUrl: 'app/pages/PowerBi_NTL/ThucThu/ThucThu.html',
        // controller: 'UsersPageCtrl',
        sidebarMeta: {
          order: 0,
          icon: 'icon ion-stats'
        },
        // 'permission': 'Users.GetAll'
      });
  }

})();
