// (function () {
//   'use strict';

//   angular.module('BlurAdmin.pages.system')
//     .controller('UsersPageCtrl', UsersPageCtrl);

//   /** @ngInject */
//   function UsersPageCtrl($scope, apiService, confirmService) {

//     $scope.users = {
//       isLoading: true,
//       tableState: null,
//       tablePageSize: 10,
//       tableData: [],
//       init: function (tableState) {
//         $scope.users.tableState = tableState;
//         $scope.users.refresh();
//       },
//       refresh: function () {
//         var _this = this;
//         _this.isLoading = true;
//         apiService.get('users', _this.tableState, function (response) {
//           _this.tableData = response.data.data;
//           _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
//           _this.tableState.pagination.totalItemCount = response.data.totalRecords;
//           _this.isLoading = false;
//         }, function() {
//           _this.isLoading = false;
//         });
//       },
//       delete: function (userId) {
//         var _this = this;
//         confirmService.showTranslate('YOU_ARE_SURE_WANT_TO_DELETE', function() {
//           apiService.delete('users/' + userId, function (response) {
//             _this.refresh();
//           });
//         });
//       }
//     };
//   }

// })();
