(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_NTL')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_NTL.L8', {
        url: '/L8',
        title: 'L8',
        templateUrl: 'app/pages/PowerBi_NTL/L8/L8.html',
        // controller: 'UsersPageCtrl',
        sidebarMeta: {
          order: 0,
          // icon: 'ion-ios-more'
        },
        // 'permission': 'Users.GetAll'
      });
  }

})();
