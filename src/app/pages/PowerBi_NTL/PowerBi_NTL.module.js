(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_NTL', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_NTL', {
        url: '/PowerBi_NTL',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Báo cáo NTL',
        sidebarMeta: {
          order: 5,
          icon: 'icon ion-stats-bars'
        },
        // 'permission': ['Users.GetAll', 'Groups.GetAll']
      });
  }

})();
