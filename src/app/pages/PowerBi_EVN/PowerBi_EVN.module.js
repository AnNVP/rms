(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_EVN', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_EVN', {
        url: '/PowerBi_EVN',
        templateUrl: 'app/pages/PowerBi_EVN/PowerBi_EVN.html',
        title: 'Báo cáo EVN',
        sidebarMeta: {
          icon: 'icon ion-stats-bars',
          order: 5,
        },
      });
  }

})();