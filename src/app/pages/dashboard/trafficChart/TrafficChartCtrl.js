/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.dashboard')
        .controller('TrafficChartCtrl', TrafficChartCtrl);

    /** @ngInject */
    function TrafficChartCtrl($scope, baConfig, colorHelper, $timeout, apiService) {
        $scope.$watch('datePicker.date.startDate', function (newValue, oldValue) {
            loadCharts();
        })
        $scope.$watch('datePicker.date.endDate', function (newValue, oldValue) {
            loadCharts();
        })


        function loadCharts() {
            var params = {
                ngayBatDau: $scope.datePicker.date.startDate.toDate(),
                ngayKetThuc: $scope.datePicker.date.endDate.toDate()
            }
            apiService.get('Dashboard', params, function (response) {
               

                $scope.transparent = baConfig.theme.blur;
                var dashboardColors = baConfig.colors.dashboard;
                $scope.giaTriCacKenh = response.data.giaTriCacKenh;
                var percentKenhSMS = (response.data.giaTriKenhSms / response.data.giaTriCacKenh) * 100;
                var percentKenhPhieuThuTienMat = (response.data.giaTriKenhPhieuThuTienMat / response.data.giaTriCacKenh) * 100;
                var percentKenhCod = (response.data.giaTriKenhCod / response.data.giaTriCacKenh) * 100;
                var percentKenhPayment = (response.data.giaTriKenhPayment / response.data.giaTriCacKenh) * 100;
                var percentKenhAtm = (response.data.giaTriKenhAtm / response.data.giaTriCacKenh) * 100;
                var percentKenhPos = (response.data.giaTriKenhPos / response.data.giaTriCacKenh) * 100;
                var percentKenhHcm = 100 - percentKenhSMS - percentKenhPhieuThuTienMat - percentKenhPayment - percentKenhCod - percentKenhAtm - percentKenhPos;
                $scope.dataValue = [response.data.giaTriKenhSms, response.data.giaTriKenhPhieuThuTienMat, response.data.giaTriKenhCod, response.data.giaTriKenhPayment, response.data.giaTriKenhPos, response.data.giaTriKenhAtm, response.data.giaTriKenhHcm];
                $scope.dataPercent = [percentKenhSMS, percentKenhPhieuThuTienMat, percentKenhCod, percentKenhPayment, percentKenhPos, percentKenhAtm, percentKenhHcm];

                $scope.doughnutData = {
                    labels: [
                        'SMS',
                        'Tiền mặt HQ',
                        'COD',
                        'Payment',
                        'POS',
                        'ATM',
                        'Tiền mặt SN'
                    ],
                    datasets: [{
                        data: [response.data.giaTriKenhSms, response.data.giaTriKenhPhieuThuTienMat, response.data.giaTriKenhCod, response.data.giaTriKenhPayment, response.data.giaTriKenhPos, response.data.giaTriKenhAtm, response.data.giaTriKenhHcm],
                        backgroundColor: [
                            dashboardColors.white,
                            dashboardColors.blueStone,
                            dashboardColors.surfieGreen,
                            dashboardColors.silverTree,
                            dashboardColors.gossip,
                            dashboardColors.blueStone1,
                            dashboardColors.green
                        ],
                        hoverBackgroundColor: [
                            colorHelper.shade(dashboardColors.white, 15),
                            colorHelper.shade(dashboardColors.blueStone, 15),
                            colorHelper.shade(dashboardColors.surfieGreen, 15),
                            colorHelper.shade(dashboardColors.silverTree, 15),
                            colorHelper.shade(dashboardColors.gossip, 15),
                            colorHelper.shade(dashboardColors.blueStone1, 15),
                            colorHelper.shade(dashboardColors.green, 15)
                        ],
                        percentage: [percentKenhSMS, percentKenhPhieuThuTienMat, percentKenhCod, percentKenhPayment, percentKenhPos, percentKenhAtm, percentKenhHcm]
                    }]
                };

                var ctx = document.getElementById("chart-area").getContext("2d");
                if (window.myDoughnut != null) {
                     window.myDoughnut.destroy();

                     
                }
            

                window.myDoughnut = new Chart(ctx, {
                    type: 'doughnut',
                    data: $scope.doughnutData,
                    options: {
                        title: {
                            display: false,
                            text: 'DOANH SỐ THEO KÊNH BÁN',
                            fontStyle: 'bold',
                            fontSize: 20
                        },
                        cutoutPercentage: 64,
                        responsive: true,

                        //   elements: {
                        //       arc: {
                        //           borderWidth: 0
                        //       }
                        //   }


                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    var dataLabel = data.labels[tooltipItem.index];
                                    var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                                    if (Chart.helpers.isArray(dataLabel)) {
                                        dataLabel = dataLabel.slice();
                                        dataLabel[0] += value;
                                    } else {
                                        dataLabel += value;
                                    }
                                    return dataLabel;
                                }
                            }
                        }
                    },

                });
            })
        }



    }
})();