/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.morris')
    .directive('morrisCtrl', morrisCtrl);
  /** @ngInject */
  function morrisCtrl() {
    return {
      restrict: 'E',
      scope: {
        datePicker: '=',
      },
      controller: 'morrisCtrl',
      templateUrl: 'app/pages/charts/chartJs/morris.html',
   
    };
  }
})();

