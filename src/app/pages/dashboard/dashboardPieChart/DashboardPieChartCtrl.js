/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .controller('DashboardPieChartCtrl', DashboardPieChartCtrl);

  /** @ngInject */
  function DashboardPieChartCtrl($scope, $timeout, apiService) {

    $scope.$watch('datePicker.date.startDate', function(newValue, oldValue) {
      updatePieCharts();
    })
    $scope.$watch('datePicker.date.endDate', function(newValue, oldValue) {
      updatePieCharts();
    })

    function loadPieCharts() {

      $('.chart').each(function () {
        var chart = $(this);
        chart.easyPieChart({
          easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: chart.attr('rel'),
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });
      });
    }

    function updatePieCharts() {
      var params = {
        ngayBatDau: $scope.datePicker.date.startDate.toDate(),
        ngayKetThuc: $scope.datePicker.date.endDate.toDate()
      }
      apiService.get('Dashboard', params, function(response) {
        $scope.data = {
          tongDonHang: response.data.tongDonHang,
          tongDonHangDaHoanThanh: response.data.tongDonHangDaHoanThanh,
          tongDonHangChuaHoanThanh: response.data.tongDonHangChuaHoanThanh,
          tongDonHangMoi: response.data.tongDonHangMoi
        };
        var percentHoanThanh = (response.data.tongDonHangDaHoanThanh / response.data.tongDonHang) * 100;
        var percentChuaHoanThanh = (response.data.tongDonHangChuaHoanThanh / response.data.tongDonHang) * 100;
        var tongDonHangMoi = 100 - percentHoanThanh - percentChuaHoanThanh;
        var tongDonHang = response.data.tongDonHang;
        
        $('.chartTongDonHang').data('easyPieChart').update(tongDonHang > 0 ? 100 : 0);
        $('.chartTongDonHangChuaHoanThanh').data('easyPieChart').update(tongDonHang > 0 ? percentChuaHoanThanh : 0 );
        $('.chartTongDonHangDaHoanThanh').data('easyPieChart').update(tongDonHang > 0 ? percentHoanThanh : 0);
        $('.chartTongDonHangDangCho').data('easyPieChart').update(tongDonHang > 0 ? tongDonHangMoi : 0);
      })
    }

    $timeout(function () {
      updatePieCharts();
      loadPieCharts();

    }, 0);
  }
})();