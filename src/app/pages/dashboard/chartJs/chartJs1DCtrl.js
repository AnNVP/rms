/**
 * @author a.demeshko
 * created on 12/16/15
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.chartJs')
    .controller('chartJs1DCtrl', chartJs1DCtrl);

  /** @ngInject */
  function chartJs1DCtrl($scope, apiService, baConfig) {


    var layoutColors = baConfig.colors;
    apiService.get('Dashboard', null, function (response) {
      $scope.labels = ["ACB", "Argibank", "Sacombank", "Techcombank", "Vietcombank", "Vietinbank", "VPbank"];
      $scope.data = [response.data.giaTriNganHangACB, response.data.giaTriNganHangArgibank, response.data.giaTriNganHangSacombank,
      response.data.giaTriNganHangTechcombank, response.data.giaTriNganHangVietcombank,
      response.data.giaTriNganHangVietinbank, response.data.giaTriNganHangVPbank];
      $scope.options = {
        // elements: {
        //   arc: {
        //     borderWidth: 0
        //   }
        // },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            fontColor: layoutColors.defaultText
          }
        },
        elements: {
          arc: {
            borderWidth: 1,
          }
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var dataLabel = data.labels[tooltipItem.index];
              var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();


              if (Chart.helpers.isArray(dataLabel)) {

                dataLabel = dataLabel.slice();
                dataLabel[0] += value;
              } else {
                dataLabel += value;
              }
              return dataLabel;
            }
          }

        }
      };
    })

  }

})();