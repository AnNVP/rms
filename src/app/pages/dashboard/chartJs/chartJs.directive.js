/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.chartJs')
    .directive('chartJs1DCtrl', chartJs1DCtrl);
  /** @ngInject */
  function chartJs1DCtrl() {
    return {
      restrict: 'E',
      scope: {
  
      },
      controller: 'chartJs1DCtrl',
      templateUrl: 'app/pages/charts/chartJs/chartJs.html',
   
    };
  }
})();