/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.amCharts')
      .controller('PieChartCtrl', PieChartCtrl);

  /** @ngInject */
  function PieChartCtrl($element, layoutPaths, baConfig) {
       $scope.$watch('datePicker.date.startDate', function (newValue, oldValue) {
         loadCharts();
       })
       $scope.$watch('datePicker.date.endDate', function (newValue, oldValue) {
         loadCharts();
       })

        function loadCharts() {
          var params = {
            ngayBatDau: $scope.datePicker.date.startDate.toDate(),
            ngayKetThuc: $scope.datePicker.date.endDate.toDate()
          }
          apiService.get('Dashboard', params, function (response) {

                $scope.transparent = baConfig.theme.blur;
                var dashboardColors = baConfig.colors.dashboard;
                $scope.giaTriCacKenh = response.data.giaTriCacKenh;
                var percentKenhSMS = (response.data.giaTriKenhSms / response.data.giaTriCacKenh) * 100;
                var percentKenhPhieuThuTienMat = (response.data.giaTriKenhPhieuThuTienMat / response.data.giaTriCacKenh) * 100;
                var percentKenhCod = (response.data.giaTriKenhCod / response.data.giaTriCacKenh) * 100;
                var percentKenhPayment = (response.data.giaTriKenhPayment / response.data.giaTriCacKenh) * 100;
                var percentKenhAtm = (response.data.giaTriKenhAtm / response.data.giaTriCacKenh) * 100;
                console.log(percentKenhAtm);
                var percentKenhPos = (response.data.giaTriKenhPos / response.data.giaTriCacKenh) * 100;
                console.log(percentKenhPos);
                var percentKenhHcm = 100 - percentKenhSMS - percentKenhPhieuThuTienMat - percentKenhPayment - percentKenhCod - percentKenhAtm - percentKenhPos;

    var layoutColors = baConfig.colors;
    var id = $element[0].getAttribute('id');
    var pieChart = AmCharts.makeChart(id, {
      type: 'pie',
      startDuration: 0,
      theme: 'blur',
      addClassNames: true,
      color: layoutColors.defaultText,
      labelTickColor: layoutColors.borderDark,
      legend: {
        position: 'right',
        marginRight: 50,
        autoMargins: false,
      },
      innerRadius: '40%',
      defs: {
        filter: [
          {
            id: 'shadow',
            width: '200%',
            height: '200%',
            feOffset: {
              result: 'offOut',
              in: 'SourceAlpha',
              dx: 0,
              dy: 0
            },
            feGaussianBlur: {
              result: 'blurOut',
              in: 'offOut',
              stdDeviation: 5
            },
            feBlend: {
              in: 'SourceGraphic',
              in2: 'blurOut',
              mode: 'normal'
            }
          }
        ]
      },
      dataProvider: [
        {
          kenh: response.data.giaTriKenhSms,
          percentage: percentKenhSMS
        },
        {
          kenh: esponse.data.giaTriKenhPhieuThuTienMat,
          percentage: percentKenhPhieuThuTienMat
        },
        {
          kenh: response.data.giaTriKenhCod,
          percentage: percentKenhCod
        },
        {
          kenh: response.data.giaTriKenhPayment,
          percentage: percentKenhPayment
        },
        {
          kenh: response.data.giaTriKenhPos,
          percentage: percentKenhPos
        },
        {
          kenh: response.data.giaTriKenhAtm,
          percentage: percentKenhAtm
        },
        {
          kenh: response.data.percentKenhHcm,
          percentage: percentKenhHcm
        }
      ],
      valueField: 'percentage',
      titleField: 'kenh',
      export: {
        enabled: true
      },
      creditsPosition: 'bottom-left',

      autoMargins: false,
      marginTop: 10,
      alpha: 0.8,
      marginBottom: 0,
      marginLeft: 0,
      marginRight: 0,
      pullOutRadius: 0,
      pathToImages: layoutPaths.images.amChart,
      responsive: {
        enabled: true,
        rules: [
          // at 900px wide, we hide legend
          {
            maxWidth: 600,
            overrides: {
              legend: {
                enabled: false
              }
            }
          },

          // at 200 px we hide value axis labels altogether
          {
            maxWidth: 100,
            overrides: {
              valueAxes: {
                labelsEnabled: false
              },
              marginTop: 30,
              marginBottom: 30,
              marginLeft: 30,
              marginRight: 30
            }
          }
        ]
      }
    });
           })
           }
    
    pieChart.addListener('init', handleInit);

    pieChart.addListener('rollOverSlice', function (e) {
      handleRollOver(e);
    });

    function handleInit() {
      pieChart.legend.addListener('rollOverItem', handleRollOver);
    }

    function handleRollOver(e) {
      var wedge = e.dataItem.wedge.node;
      wedge.parentNode.appendChild(wedge);
    }
  }

})();
