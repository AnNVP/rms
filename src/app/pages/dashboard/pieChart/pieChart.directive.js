/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.charts.amCharts')
    .directive('pieChartCtrl', pieChartCtrl);
  /** @ngInject */
  function pieChartCtrl() {
    return {
      restrict: 'E',
      scope: {
        datePicker: '=',
      },
      controller: 'pieChartCtrl',
      templateUrl: 'app/pages/dashboard/pieChart/pieChart.html',
   
    };
  }
})();

