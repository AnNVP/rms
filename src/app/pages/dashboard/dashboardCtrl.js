(function () {
    'use strict';

    angular.module('BlurAdmin.pages.dashboard')
        .controller('dashboardCtrl', dashboardCtrl);

    /** @ngInject */
    function dashboardCtrl($scope, $translate) {
        $scope.datePicker = {
            date: {
                startDate: moment().startOf('month'),
                endDate: moment().endOf('month')
            },
            options: {
                autoApply: true,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            },

        };
    }
})();