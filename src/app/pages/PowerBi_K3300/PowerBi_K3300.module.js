(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_K3300', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_K3300', {
        url: '/PowerBi_K3300',
        templateUrl: 'app/pages/PowerBi_K3300/PowerBi_K3300.html',
        title: 'Báo cáo K3300',
        sidebarMeta: {
          icon: 'icon ion-cash',
          order: 1,
        },
      });
  }

})();