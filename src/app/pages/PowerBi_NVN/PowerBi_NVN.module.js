(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_NVN', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_NVN', {
        url: '/PowerBi_NVN',
        templateUrl: 'app/pages/PowerBi_NVN/PowerBi_NVN.html',
        title: 'Báo cáo NVN',
        sidebarMeta: {
          icon: 'icon ion-stats-bars',
          order: 3,
        },
      });
  }

})();