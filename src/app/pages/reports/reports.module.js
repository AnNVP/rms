(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reports', {
        url: '/reports',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Reports',
        sidebarMeta: {
          icon: 'ion-settings',
          order: 3,
        },
        'permission': ['Reports.s3300','S200.Get_S200','S100Report.GetListS100Report','S500.Get_S500']
      });
  }

})();
