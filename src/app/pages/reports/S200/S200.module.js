(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reports.S200', {
        url: '/S200',
        title: 'S200',
        templateUrl: 'app/pages/reports/S200/S200.html',
        controller: 'S200PageCtrl',
        sidebarMeta: {
          order: 1,
        },
        'permission': 'S200.Get_S200'
      });
  }

})();
