(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .controller('S200PageCtrl', S200PageCtrl);

  /** @ngInject */

  function S200PageCtrl($scope, apiService, confirmService, $filter) {
    $scope.startDate = (moment().subtract(14, 'd')).toDate();
    $scope.endDate = moment().toDate();
    $scope.ngayXuatBaoCao = moment().toDate();
    $scope.tenBaoCao = "S200 - SSRS - BÁO CÁO THỰC THU TIỀN MẶT HÀ NỘI";
    $scope.reload = function (startDate, endDate) {
      if (startDate != null)
        $scope.startDate = startDate;
      if (endDate != null)  
        $scope.endDate = endDate;

      var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
      var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');

      $scope.S200.linkDownload= 'S200/download-template?startDate=' +startDate + '&endDate=' + endDate,
      $scope.S200.refresh();
    }
 
    var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
    var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');

    $scope.S200 = {
      isLoading: true,
      isExporting: false,
      tableState: null,
      tablePageSize: 50,
      linkDownload: 'S200/download-template?startDate=' + startDate + '&endDate=' + endDate,
      tableData: [],
      init: function (tableState) {
        $scope.S200.tableState = tableState;
        $scope.S200.refresh();
      },
      refresh: function () {
        var params = {
          ngayBatDau: $scope.startDate,
          ngayKetThuc: $scope.endDate
        }
        var _this = this;
        _this.isLoading = true;
        apiService.get('S200', params, function (response) {      
          var data = response.data;
          _this.tableData = response.data;
         // _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          _this.isLoading = false;
         
          $scope.tongtien = data.reduce(function (sum, cur) {
            return sum + (cur.soTienNop || 0 );
          }, 0);
        }, function () {
          _this.isLoading = false;
        });
      },
      download: function () {
        var _this = this;
        _this.isExporting = true;
        var fileName = 'S200_' + moment().format('YYYYMMDDHHmmss') + '.xlsx';
        apiService.download('POST', _this.linkDownload, null, fileName, function (response) {
          _this.isExporting = false;
        }, function () {
          _this.isExporting = false;
        });
      }
    };
  }

})();
