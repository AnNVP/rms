(function () {
  'use strict';
  angular.module('BlurAdmin.pages.reports')
    .controller('S100PageCtrl', S100PageCtrl);
  /** @ngInject */
  function S100PageCtrl($scope, apiService, confirmService, $filter) {
    $scope.tenBaoCao = "S100 - SSRS - BÁO CÁO THỰC THU NATIVE VIỆT NAM";
    $scope.$watch("S100.form.query", function (newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.S100.form.query = newVal;
        $scope.S100.refresh();
      }
    });
    $scope.fromDate = (moment().subtract(1, 'd')).toDate();
    $scope.toDate = moment().toDate();
    var fromDate = $filter('date')($scope.fromDate, 'yyyy-MM-dd');
    var toDate = $filter('date')($scope.toDate, 'yyyy-MM-dd');
    $scope.S100 = {
      isLoading: true,
      isExporting: false,
      tableState: null,
      tablePageSize: 10,
      form: {
        fromDate: null,
        toDate: null,
        query: null,
        paymentType: null,
        partner: null
      },
      sumSentMoney: 0,
      paymentMethod: [],
      paymentMethodSelect: 'All',
      reportTime: null,
      partner: [],
      fromDate: null,
      toDate: null,
      partnerSelect: 'All',
      linkDownload: 's100report/dowload-excel?fromDate=' + fromDate + '&toDate=' + toDate,
      init: function (tableState) {
        $scope.S100.tableState = tableState;
        apiService.get('s100report/partners',null, function(response){
          $scope.S100.partner = response.data;
          $scope.S100.partner.push('All');
          apiService.get('s100report/payment-methods',null, function(response){
            $scope.S100.paymentMethod = response.data;
            $scope.S100.paymentMethod.push('All');
          })
        })
        $scope.S100.refresh();
        
      },
      refresh: function () {
        var _this = this;
        // debugger
        _this.form.fromDate = _this.form.fromDate || moment().subtract(1, 'days').toDate();
        _this.form.toDate = _this.form.toDate || moment().toDate();
        fromDate = $filter('date')(_this.form.fromDate, 'yyyy-MM-dd HH:mm:ss');
        toDate = $filter('date')(_this.form.toDate, 'yyyy-MM-dd HH:mm:ss');
        _this.currentDay = moment().format();
        _this.limitTimeFrom = moment($scope.S100.form.toDate).subtract(30, 'days').toDate();
        _this.linkDownload = _this.updateLinkDownload(fromDate, toDate, _this.form.query, _this.form.paymentType, _this.form.partner);
        _this.isLoading = true;
        var params = {
          request: _this.tableState,
          fromDate: _this.form.fromDate,
          toDate: _this.form.toDate,
          query: _this.form.query,
          paymentType: _this.form.paymentType,
          partner: _this.form.partner
        }
        apiService.get('s100report/', params, function (response) {
          var data = response.data;
          _this.tableData = data;
          _this.sumSentMoney = data.reduce(function (sum, cur) {
            return sum + (cur.soTienNop || cur.tuitionFee || 0 );
          }, 0);
          // _this.tableState.pagination.numberOfPages = response.data.numberOfPages;
          // _this.tableState.pagination.totalItemCount = response.data.totalRecords;
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },

      paymentMethodChange: function (newVal, oldVal) {
        debugger
        var _this = this;
        if(newVal != oldVal)
        {
          _this.paymentMethodSelect = newVal;
          var paymentTypeId = _this.paymentMethod.indexOf(newVal) + 1;
          apiService.get('s100report/partner-by-payment-type/' + paymentTypeId, null, function (response) {
            if(response.data && response.data.length > 0  ){
              _this.partner = response.data;
            } else {
              _this.partner = [
                'BID - BIDV'
                , 'VCB - Vietcombank'
                , 'AGI - Agribank'
                , 'CTG - Vietin'
                , 'TCB - Tech'
                , 'VPB - VPBank'
                , 'ACB'
                , 'STB - Sacombank'
                , 'TPC-Topica'
                , 'GHTK'
                , 'GHOV'
                , 'Viettel'
              ];
            }
             
            
            if (_this.partner.length > 1) {
              _this.partnerSelect = 'All';
              _this.partner.push('All');
            } else {
              _this.partnerSelect = _this.partner[0];
            }
            _this.form.paymentType = _this.paymentMethodSelect;
            _this.form.partner = _this.partnerSelect;
           
            $scope.S100.refresh();
          })
         
        }
      },
      partnerChange: function (newVal , oldVal ) {
        if(newVal != oldVal){
          this.partnerSelect = newVal;
          this.form.partner = newVal;
          $scope.S100.refresh();
        }

      },
      timeFilterChange: function () {
        var _this = this;
        _this.limitTimeFrom = moment(_this.form.toDate).subtract(30, 'days').toDate();
        $scope.S100.refresh();
      },
      updateLinkDownload: function(fromDate, toDate, query, paymentType, partner ) {
        return 's100report/dowload-excel?fromDate=' + fromDate + '&toDate=' + toDate 
        + '&query=' + (query || '') + '&paymentType=' + (paymentType|| '') + '&partner=' + (partner|| '');
      },
      download: function () {
        var _this = this;
        _this.isExporting = true;
        var fileName = 'S100_' + moment().format('YYYYMMDDHHmmss') + '.xlsx';
        apiService.download('POST', _this.linkDownload, null, fileName, function (response) {
          _this.isExporting = false;
        }, function () {
          _this.isExporting = false;
        });
      }
    };
  }
})();
