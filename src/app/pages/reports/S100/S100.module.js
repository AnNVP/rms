(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.reports')
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('reports.S100', {
          url: '/reports/S100',
          title: 'S100',
          templateUrl: 'app/pages/reports/S100/S100.html',
          sidebarMeta: {
            order: 0,
          },
          controller: 'S100PageCtrl',
          'permission': 'S100Report.GetListS100Report'
        });
    }
  
  })();
  