(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .controller('baoCaoBanHangPageCtrl', baoCaoBanHangPageCtrl);

  /** @ngInject */

  function baoCaoBanHangPageCtrl($scope, apiService, confirmService, $filter) {
    $scope.startDate = (moment().subtract(14, 'd')).toDate();
    $scope.endDate = moment().toDate();
    $scope.ngayXuatBaoCao = moment().toDate();

    $scope.$watch("S600.form.query", function (newVal, oldVal, tableState) {

      if (newVal !== oldVal) {
        $scope.S600.form.query = newVal;
        var params = {
          ngayBatDau: $scope.startDate,
          ngayKetThuc: $scope.endDate,
          
        }

        var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
        $scope.S600.linkDownload  = 'S600/download-template?startDate=' + startDate + '&endDate=' + endDate + "&tenHocVien=" + newVal;
        apiService.get('S600/' + newVal, params, function (response) {
          var data = response.data;
          $scope.S600.tableState = tableState;
          $scope.S600.tableData = response.data;
          $scope.S600.isLoading = false;
        });     
      }
    });
    $scope.$watch("S400.form.query", function (newVal, oldVal, tableState) {

      if (newVal !== oldVal) {
        $scope.S400.form.query = newVal;
        var params = {
          ngayBatDau: $scope.startDate,
          ngayKetThuc: $scope.endDate,
          HavingTVTS: $scope.S400.form.query,
          
        }
        console.log($scope.S400.form.query);
        var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
        $scope.S400.linkDownload = 'S400/download-template?startDate=' + startDate + '&endDate=' + endDate + "&HavingTVTS=" + newVal;
        apiService.get('S400/', params, function (response) {
          var data = response.data;
          $scope.S400.tableState = tableState;
          $scope.S400.tableData = response.data;
          $scope.S400.isLoading = false;
        });     
      }
    });

   

    $scope.reload = function (startDate, endDate) {
      if (startDate != null)
        $scope.startDate = startDate;
      if (endDate != null)  
        $scope.endDate = endDate;

      var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
      var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
      $scope.S400.linkDownload = 'S400/download-template?startDate=' + startDate + '&endDate=' + endDate,
      $scope.S500.linkDownload = 'S500/download-template?startDate=' + startDate + '&endDate=' + endDate,
      $scope.S600.linkDownload = 'S600/download-template?startDate=' + startDate + '&endDate=' + endDate,
      $scope.S400.refresh();
      $scope.S500.refresh();
      $scope.S600.refresh();
    }
 
    var startDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
    var endDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');

    $scope.baoCaoBanHang = {
      isLoading: true,
      tableState: null,
      isExporting: false,
      tableData: [],
      salesBySelect: 'Chọn báo cáo',
      salesBy:['Chọn báo cáo','Tư vấn tuyển sinh', 'Gói học', 'Học viên'],
      salesByChange: function (newVal, oldVal) {
        var _this = this;
        if (newVal != oldVal) {
          _this.salesBySelect = newVal;
          if (newVal == 'Tư vấn tuyển sinh')
          {
            $scope.hideS600Search = false;
            $scope.hideS400SearchTVTS = true;
            $scope.hideS400 = true;
            $scope.hideS500 = false;
            $scope.hideS600 = false;
            $scope.hideS400Download = true;
            $scope.hideS500Download = false;
            $scope.hideS600Download = false;

            $scope.tenBaoCao = "S400 - SSRS - BÁO CÁO THU THEO TƯ VẤN TUYỂN SINH";
            $scope.S400.refresh();
          }
          else if (newVal == 'Gói học')
          {
            $scope.hideS500 = true;
            $scope.hideS400 = false;
            $scope.hideS600 = false;
            $scope.hideS500Download = true;
            $scope.hideS400Download = false;
            $scope.hideS600Download = false;
            $scope.hideS600Search = false;
                $scope.hideS400SearchTVTS = false;
            $scope.tenBaoCao = "S500 - SSRS - BÁO CÁO THU THEO GÓI HỌC";
            $scope.S500.refresh();
          }
          else if (newVal == 'Học viên')
          {
            $scope.hideS600 = true;
            $scope.hideS600Search = true;
            $scope.hideS600Download = true;
            $scope.hideS500Download = false;
            $scope.hideS400Download = false;
            $scope.hideS400 = false;
            $scope.hideS500 = false;
                $scope.hideS400SearchTVTS = false;
            $scope.tenBaoCao = "S600 - SSRS - BÁO CÁO THU THEO HỌC VIÊN";
            $scope.S600.refresh();
          }
          

        }
        else
        {
          $scope.hideS400SearchTVTS = false;
          $scope.hideS400Search = false;
          $scope.hideS400 = false;
          $scope.hideS500 = false;
          $scope.hideS600 = false;
          $scope.hideS600Search = false;
          $scope.hideS500Search = false;
          $scope.hideS400Search = false;
          $scope.hideS600Download = false;
          $scope.hideS500Download = false;
          $scope.hideS400Download = false;
          
          $scope.tenBaoCao = "";
        }
        
      },
      download: function (type) {
        debugger
        var _this = this;
        _this.isExporting = true;
        var fileName = 'S' + type + '_' + moment().format('YYYYMMDDHHmmss') + '.xlsx';
        apiService.download('POST', $scope['S'+type].linkDownload, null, fileName, function (response) {
          _this.isExporting = false;
        }, function () {
          _this.isExporting = false;
        });
      }
    };


    $scope.S400 = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      linkDownload: 'S400/download-template?startDate=' + startDate + '&endDate=' + endDate,
      tableData: [],
      init: function (tableState) {
        $scope.S400.tableState = tableState;
        $scope.S400.refresh();
      },

      refresh: function (tableState) {
        var params = {
          ngayBatDau: $scope.startDate,
          ngayKetThuc: $scope.endDate,
        }
        var _this = this;
        _this.isLoading = true;
        apiService.get('S400', params, function (response) {      
          var data = response.data;
          $scope.S400.tableState = tableState;
          _this.tableData = response.data;
          _this.isLoading = false;
          $scope.s400TienThucThu = data.reduce(function (sum, cur) {
            return sum + (cur.tongTienThucThu || 0);
          }, 0);
        }, function () {
          _this.isLoading = false;
        });
      },
    };


    $scope.S500 = {
      isLoading: true,
      tableState: null,
      tablePageSize: 10,
      linkDownload: 'S500/download-template?startDate=' + startDate + '&endDate=' + endDate,
      tableData: [],
      init: function (tableState) {
        // $scope.S500.tableState = tableState;
        // $scope.S500.refresh();
      },

      refresh: function (tableState) {
        
        var params = {
          ngayBatDau: $scope.startDate,
          ngayKetThuc: $scope.endDate
        }
        var _this = this;
        _this.isLoading = true;
        apiService.get('S500', params, function (response) {
          var data = response.data;
          $scope.S500.tableState = tableState;
          _this.tableData = response.data;
          _this.isLoading = false;
          $scope.s500TienThucThu = data.reduce(function (sum, cur) {
            return sum + (cur.tongTienThucThu || 0);
          }, 0);
        }, function () {
          _this.isLoading = false;
        });
      },
    };
 

  $scope.S600 = {
    isLoading: true,
    tableState: null,
    tablePageSize: 10,
    linkDownload: 'S600/download-template?startDate=' + startDate + '&endDate=' + endDate,
    tableData: [],

    refresh: function (tableState) {

      var params = {
        ngayBatDau: $scope.startDate,
        ngayKetThuc: $scope.endDate
      }
      var _this = this;
      _this.isLoading = true;
      apiService.get('S600', params, function (response) {
        var data = response.data;
        $scope.S600.tableState = tableState;
        _this.tableData = response.data;
        _this.isLoading = false;
        $scope.s600TienThucThu = data.reduce(function (sum, cur) {
          return sum + (cur.tongTienThucThu || 0);
        }, 0)
      }, function () {
        _this.isLoading = false;
      });
    },
  };
}

})();
