(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reports.baoCaoBanHang', {
        url: '/baoCaoBanHang',
        title: 'Báo cáo bán hàng',
        templateUrl: 'app/pages/reports/baoCaoBanHang/baoCaoBanHang.html',
        controller: 'baoCaoBanHangPageCtrl',
        sidebarMeta: {
          order: 4,
        },
        'permission': 'S500.Get_S500'
      });
  }

})();
