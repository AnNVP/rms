(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .controller('S3300PageCtrl', S3300PageCtrl);

  /** @ngInject */

  function S3300PageCtrl($http, $scope, datetimeService, apiService, notificationService) {

    var now = moment();
    $scope.tenBaoCao = "S3300 - SSRS - BÁO CÁO TỔNG THỰC THU";
    $scope.s3300 = {
      isLoading: true,
      isExporting: false,
      datePicker: {
        date: { startDate: now, endDate: now },
        options: {
          autoApply: true,
          // maxSpan: {
          //   days: 30
          // },
          eventHandlers: {
            'apply.daterangepicker': function () {
              $scope.s3300.refresh();
            }
          }
        },
      },
      data: [],
      init: function () {
        $scope.s3300.refresh();
      },
      refresh: function () {
        var _this = this;
        var date = _.clone(_this.datePicker.date);

        if (date.endDate.diff(date.startDate, 'days') >= 30) {
          notificationService.errorTranslate('VUI_LONG_CHI_CHON_TOI_DA_30_NGAY_CHO_BAO_CAO');
          return false;
        }

        _this.isLoading = true;
        var params = {
          start: date.startDate.toDate(),
          end: date.endDate.toDate()
        };
        
        apiService.get('reports/s3300', params, function (response) {
          _this.data = response.data;
          var selectedDays = datetimeService.dateRange(date.startDate.toDate(), date.endDate.toDate());
          var lengthSelectedDays = selectedDays.length;
          _.forEach(_this.data, function(items, index) {
            if (items.length !== lengthSelectedDays) {
              _.forEach(selectedDays, function (d) {
                if (!_.find(items, function (o) { return moment(d).startOf('day').isSame(o.date); })) {
                  items.push({ date: d});
                }
              });
            }
            _.map(items, function (o) { o.date = new Date(o.date); });
            _this.data[index] = _.orderBy(items, ['date']);
          });
          _this.isLoading = false;
        }, function () {
          _this.isLoading = false;
        });
      },
      sum: function(index) {
        return _.sumBy(this.data[index], function (o) { return o.money; });
      },
      download: function() {
        var _this = this;
        _this.isExporting = true;
        var date = _.clone(_this.datePicker.date);
        var fileName = 'S3300_' + date.startDate.format('YYYYMMDD')
             + '_' + date.endDate.format('YYYYMMDD') + '.xlsx';
        apiService.download('POST', 'reports/download-s3300', _this.data, fileName, function (response) {
          _this.isExporting = false;
        }, function() {
          _this.isExporting = false;
        });
      }
    };
  }

})();
