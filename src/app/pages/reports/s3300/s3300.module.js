(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reports')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('reports.s3300', {
        url: '/s3300',
        title: 'S3300',
        templateUrl: 'app/pages/reports/s3300/s3300.html',
        controller: 'S3300PageCtrl',
        sidebarMeta: {
          order: 3,
        },
        'permission': 'Reports.S3300'
      });
  }

  
})();
