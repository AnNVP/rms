(function () {
  'use strict';

  angular.module('BlurAdmin.pages.PowerBi_TG', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('PowerBi_TG', {
        url: '/PowerBi_TG',
        templateUrl: 'app/pages/PowerBi_TG/PowerBi_TG.html',
        title: 'Báo cáo thực thu TG',
        sidebarMeta: {
          icon: 'icon ion-cash',
          order: 1,
        },
      });
  }

})();