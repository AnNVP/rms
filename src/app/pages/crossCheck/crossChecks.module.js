(function () {
  'use strict';

  angular.module('BlurAdmin.pages.crossChecks', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('crossCheck', {
        url: '/cross-check',
        template: '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'CROSS_CHECK',
        sidebarMeta: {
          icon: 'ion-settings',
          order: 4,
        },
        'permission': ['CrossChecks.CodImport']
      });
  }

})();
