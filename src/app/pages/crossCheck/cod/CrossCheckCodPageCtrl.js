(function () {
  'use strict';
  angular.module('BlurAdmin.pages.crossChecks')
    .controller('CrossCheckCodPageCtrl', CrossCheckCodPageCtrl);

  /** @ngInject */

  function CrossCheckCodPageCtrl($scope, $timeout, Upload, apiService, notificationService, config, composeModal) {
    $scope.sumExcelMoney = 0;
    $scope.sumSsrsMoney = 0;
    $scope.crossCheckCod = {
      isShowTable: false,
      isImporting: false,
      isShowImport: true,
      selection: [],
      isSelectAll: false,
      isHideCheckAll: false,
      linkDownload: config.apiUrl + 'cross-checks/cod/download-template',
      datePicker: {
        date: { startDate: moment().subtract(6, 'd'), endDate: moment() },
        options: {
          autoApply: true,
          eventHandlers: {
            'apply.daterangepicker': function () {
              var date = $scope.crossCheckCod.datePicker.date;
              if (date.endDate.diff(date.startDate, 'days') >= 32) {
                $scope.crossCheckCod.isShowImport = false;
                notificationService.errorTranslate('VUI_LONG_CHI_CHON_TOI_DA_31_NGAY_CHO_BAO_CAO');
              } else {
                $scope.crossCheckCod.isShowImport = true;
              }
            }
          }
        },
      },
      tableData: [],
      import: function (file, errFiles) {
        var _this = this;

        if (!_.isUndefined(errFiles && errFiles[0])) {
          notificationService.error(errFiles[0].$error + ' ' + errFiles[0].$errorParam);
          return false;
        }

        if (file) {
          _this.isShowTable = false;
          _this.isImporting = true;
          file.upload = Upload.upload({
            url: config.apiUrl + 'cross-checks/cod/import',
            data: {
              file: file,
              start: _this.datePicker.date.startDate.toDate(),
              end: _this.datePicker.date.endDate.toDate()
            }
          });

          file.upload.then(function (response) {
            _this.tableData = response.data.data;
            $timeout(function () {
              notificationService.successTranslate('SUCCESS');
              $scope.sumExcelMoney = 0;
              $scope.sumSsrsMoney = 0;
              _this.selection = [];
              _this.isHideCheckAll = false;
              _this.tableData.forEach(function(element){
                if(element.status == 1){
                  element.checked = true;
                  _this.selection.push(element.crossCheckId);
                  $scope.sumSsrsMoney += (element.money || 0);
                };
                $scope.sumExcelMoney += (element.data.actualReceivedMoney || 0);
              });
              
              if(_this.selection.length === 0)
              {
                _this.isHideCheckAll = true;
              }
              _this.isSelectAll = true;
              _this.isImporting = false;
              _this.isShowTable = true;
            });
          }, function (response) {
            if (_.has(response, 'data.message')) {
              notificationService.errorTranslate(response.data.message);
            } else {
              notificationService.errorTranslate('ERROR_OCCURRED');
            }
            _this.isImporting = false;
          });
        }
      },

      selectAll: function (collection) {
        var _this = this;
        if (_this.selection.length === 0) {
          $scope.sumSsrsMoney = 0;
          angular.forEach(collection, function (item) {
            if(item.status == 1){
              _this.selection.push(item.crossCheckId);
              $scope.sumSsrsMoney += (item.money || 0);
              item.checked = true;
            }
          });
          _this.isSelectAll = true;
        } else if (_this.selection.length > 0 && _this.selection.length != _.filter(this.tableData, function (o) { return o.status === 1; }).length) {
          angular.forEach(collection, function (item) {
            if(item.status == 1){
              var found = _this.selection.indexOf(item.crossCheckId);
              if (found == -1)
              {
                _this.selection.push(item.crossCheckId);
                $scope.sumSsrsMoney += (item.money || 0);  
              }
              item.checked = true;
            }          
          });
          _this.isSelectAll = true;
        } else {
          _this.selection = [];
          angular.forEach(collection, function (item) {
            item.checked = false;
            $scope.sumSsrsMoney = 0;
          });
          _this.isSelectAll = false;
        }
      },
      toggleSelection: function (id) {
        var idx = this.selection.indexOf(id);
        var crossCheckCod = _.filter(this.tableData, function (o) { return o.crossCheckId === id; })[0];
        if (idx > -1) {
          crossCheckCod.checked = false;
          this.selection.splice(idx, 1);
          $scope.sumSsrsMoney -= (this.tableData[id - 1].money || 0); 
          this.isSelectAll = false;
        } else {
          crossCheckCod.checked = true;
          this.selection.push(id);
          $scope.sumSsrsMoney += (this.tableData[id - 1].money || 0); 
          if(this.selection.length === _.filter(this.tableData, function (o) { return o.status === 1; }).length){
            this.isSelectAll = true;
          }
        } 
      },

      createGroupCode: function () { 
        var _this = this;
        if (_this.selection.length === 0) {
          notificationService.errorTranslate('PLEASE_CHECK_ATLEAST_ONE_VALUE');
          return false;
        }
        var params = {  };
        var codIds = [];
        _this.selection.forEach(function(id){
          var cod =  _this.tableData.find(function(element){
            return element.crossCheckId === id;
          })
          if(cod && cod.data) codIds.push(cod.data.id);
        })  

        params.Selections = codIds;
        apiService.create('cross-checks/create-group-code' , params, function (response) {
          var groupCode = response.data.groupCode;
          if(groupCode != null){
            groupCode = groupCode.toUpperCase();
          }
          var startDate = moment(_this.datePicker.date.startDate.toDate()).format("DD/MM/YYYY");
          var endDate =  moment(_this.datePicker.date.endDate.toDate()).format("DD/MM/YYYY"); 
          //var sumMoneyChar = _this.SoTienKieuChu(String($scope.sumSsrsMoney));
          var sumMoney = $scope.sumSsrsMoney.toLocaleString('en-GB');
          composeModal.open({
            subject : '[TOPICA NATIVE] Nội dung chuyển khoản biên bản đối soát từ ngày ' + startDate + ' đến ngày ' + endDate ,
            to: '',
            text: 'Dear kế toán, <p>Gửi anh/chị nội dung đối soát từ ngày <strong> ' + startDate + '</strong> đến ngày <strong>' + endDate + '</strong></p>'
            + '<p> Số tiền chuyển: <strong>' + sumMoney  + ' VND </strong></p>'
            + '<p> Anh/ chị chuyển khoản biên bản đối soát theo cú pháp:</p> '
            + '<p> Ong vàng: OVA GC' + groupCode + '_STGD <strong>' + $scope.sumSsrsMoney + 'VND </strong></p>'
            + '<p> Giao hàng tiết kiệm: GHT GC' + groupCode + '_STGD <strong>' + $scope.sumSsrsMoney + 'VND </strong></p>' 
            + '<p> Địa phương Hồ Chí Minh: HCM GC' + groupCode + '</p>'
            
          })
          //_this.sendGroupCodeDialog('Test', groupCode, _this.sendMail, 0 );

        });
        
      },

      SoTienKieuChu: function(number){
        var dv = ["", "mươi", "trăm", "nghìn", "triệu", "tỉ"];
        var cs = ["không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" ]
        var doc;
        var i, j, k, n, len, found, ddv, rd;

        len = number.length;
        number += "ss";
        doc = "";
        found = 0;
        ddv = 0;
        rd = 0;

        i = 0;
        while (i < len)
        {
            //So chu so o hang dang duyet
            n = (len - i + 2) % 3 + 1;

            //Kiem tra so 0
            found = 0;
            for (j = 0; j < n; j++)
            {
                if (number[i + j] != '0')
                {
                    found = 1;
                    break;
                }
            }

            //Duyet n chu so
            if (found == 1)
            {
                rd = 1;
                for (j = 0; j < n; j++)
                {
                    ddv = 1;
                    switch (number[i + j])
                    {
                        case '0':
                            if (n - j == 3)
                                doc += cs[0] + " ";
                            if (n - j == 2)
                            {
                                if (number[i + j + 1] != '0')
                                    doc += "lẻ ";
                                ddv = 0;
                            }
                            break;
                        case '1':
                            if (n - j == 3)
                                doc += cs[1] + " ";
                            if (n - j == 2)
                            {
                                doc += "mười ";
                                ddv = 0;
                            }
                            if (n - j == 1)
                            {
                                if (i + j == 0)
                                    k = 0;
                                else
                                    k = i + j - 1;

                                if (number[k] != '1' && number[k] != '0')
                                    doc += "mốt";
                                else
                                    doc += cs[1] + " ";
                            }
                            break;
                        case '5':
                            if (n - j == 3)
                                doc += cs[5] + " ";
                            if (n - j == 2)
                            {
                                doc += "năm mươi ";
                                ddv = 0;
                            }
                            if (n - j == 1)
                            {
                                if (i + j == 0)
                                    k = 0;
                                else
                                    k = i + j - 1;

                                if (number[k] != '0')
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                            }
                            break;
                        default:
                            doc += cs[number[i + j] - 48] + " ";
                            break;
                    }

                    //Doc don vi nho
                    if (ddv == 1)
                    {
                        doc += dv[n - j - 1] + " ";
                    }
                }
            }


            //Doc don vi lon
            if (len - i - n > 0)
            {
                if ((len - i - n) % 9 == 0)
                {
                    if (rd == 1)
                        for (k = 0; k < (len - i - n) / 9; k++)
                            doc += "tỉ ";
                    rd = 0;
                }
                else if (found != 0)
                    doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
            }

            i += n;
        }
        if (len == 1)
          if (number[0] == '0' || number[0] == '5')
            return cs[number[0] - 48];

        return doc + "đồng";
      }
    };
  }

})();
