/**
 * @author a.demeshko
 * created on 24/12/15
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.crossChecks')
    .controller('composeBoxCtrl', composeBoxCtrl);

  /** @ngInject */
  function composeBoxCtrl(subject, to, text, $scope, apiService) {
    var vm = this;
    vm.subject = subject;
    vm.to = to;
    vm.text = text;
    $scope.send = function() {  
      var _to = this.boxCtrl.to;
      var _subject = this.boxCtrl.subject;
      var _contents = this.boxCtrl.text;
      var receivers = _to.split(',');
      for(var i =0 ; i < receivers.length; i++){
        if(!$scope.validateEmail(receivers[i])){
          receivers.splice(i,1);
        }
      }
      var params = {
        receivers: receivers,
        subject: subject,
        contents: text
      };
      apiService.create('cross-checks/crosscheck-send-mail',params, function(response){
        $scope.$dismiss();
      })
    },

    $scope.validateEmail = function(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
  }
})();