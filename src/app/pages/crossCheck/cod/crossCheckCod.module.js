(function () {
  'use strict';

  angular.module('BlurAdmin.pages.crossChecks')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('crossCheck.cod', {
        url: '/cod',
        title: 'COD',
        templateUrl: 'app/pages/crossCheck/cod/crossCheckCod.html',
        controller: 'CrossCheckCodPageCtrl',
        sidebarMeta: {
          order: 3,
        },
        // 'permission': 'Reports.S3300'
      });
  }

})();
