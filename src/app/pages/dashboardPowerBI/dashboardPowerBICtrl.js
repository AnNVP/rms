(function () {
    'use strict';
    angular.module('BlurAdmin.pages.dashboardPowerBI')
        .controller('dashboardPowerBICtrl', dashboardPowerBICtrl);
    /** @ngInject */
    function dashboardPowerBICtrl($scope, $filter, apiService) {
        $scope.datePicker = {
            date: {
                startDate: moment().startOf('month'),
                endDate: moment().endOf('month')
            },
            options: {
                autoApply: true,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            },
        }
        $scope.itemList = [];
        $scope.heSoLuongDatas = [{
            id: 1,
            value: "0.10"
        }, {
            id: 2,
            value: "0.20"
        }, {
            id: 3,
            value: "0.30"
        }, {
            id: 4,
            value: "0.40"
        }, {
            id: 5,
            value: "0.50"
        }, {
            id: 6,
            value: "0.60"
        }, {
            id: 7,
            value: "0.70"
        }, {
            id: 8,
            value: "0.80"
        }, {
            id: 9,
            value: "0.90"
        }, {
            id: 10,
            value: "1.00"
        }]
        $scope.heSoThoiGianDatas = [{
            id: 1,
            value: "0.10"
        }, {
            id: 2,
            value: "0.20"
        }, {
            id: 3,
            value: "0.30"
        }, {
            id: 4,
            value: "0.40"
        }, {
            id: 5,
            value: "0.50"
        }, {
            id: 6,
            value: "0.60"
        }, {
            id: 7,
            value: "0.70"
        }, {
            id: 8,
            value: "0.80"
        }, {
            id: 9,
            value: "0.90"
        }, {
            id: 10,
            value: "1.00"
        }]
        $scope.heSoChatLuongDatas = [{
            id: 1,
            value: "0.10"
        }, {
            id: 2,
            value: "0.20"
        }, {
            id: 3,
            value: "0.30"
        }, {
            id: 4,
            value: "0.40"
        }, {
            id: 5,
            value: "0.50"
        }, {
            id: 6,
            value: "0.60"
        }, {
            id: 7,
            value: "0.70"
        }, {
            id: 8,
            value: "0.80"
        }, {
            id: 9,
            value: "0.90"
        }, {
            id: 10,
            value: "1.00"
        }]
        $scope.phanTramCOMDatas = [{
            id: 1,
            value: "0.00"
        }, {
            id: 2,
            value: "0.25"
        }, {
            id: 3,
            value: "0.50"
        }, {
            id: 4,
            value: "0.75"
        }, {
            id: 5,
            value: "1.00"
        }, {
            id: 6,
            value: "1.25"
        }, {
            id: 7,
            value: "1.50"
        }, {
            id: 8,
            value: "1.75"
        }, {
            id: 9,
            value: "2.00"
        }, {
            id: 10,
            value: "2.25"
        }, {
            id: 11,
            value: "2.50"
        }, {
            id: 12,
            value: "2.75"
        }, {
            id: 13,
            value: "3.00"
        }, {
            id: 14,
            value: "3.25"
        }, {
            id: 15,
            value: "3.50"
        }, {
            id: 16,
            value: "3.75"
        }, {
            id: 17,
            value: "4.00"
        }, {
            id: 18,
            value: "4.25"
        }, {
            id: 19,
            value: "4.50"
        }, {
            id: 20,
            value: "4.75"
        }, {
            id: 21,
            value: "5.00"
        }]
        $scope.heSoLuong = $scope.heSoLuongDatas[9];
        $scope.heSoThoiGian = $scope.heSoThoiGianDatas[9];
        $scope.heSoChatLuong = $scope.heSoChatLuongDatas[9];
        $scope.phanTramCOM = $scope.phanTramCOMDatas[12];
        $scope.changedValueHSL = function (item) {
            return item.value;
        }
        $scope.changedValueHSTG = function (item) {
            return item.value;
        }
        $scope.$watch('datePicker.date.startDate', function (newValue, oldValue) {
            Refresh();
        })
        $scope.$watch('datePicker.date.endDate', function (newValue, oldValue) {
            Refresh();
        })
        function Refresh() {
            apiService.get('oauth/info', null, function (response) {
                $scope.email = response.data.email;
                var params = {
                    ngayBatDau: $scope.datePicker.date.startDate.toDate(),
                    ngayKetThuc: $scope.datePicker.date.endDate.toDate(),
                    tvts: response.data.email
                
                }
                apiService.get('DashboardPowerBI/get-actual-received-money', params, function (response) {
                    if (response.data.sumActualReceivedMoney != null) {
                        $scope.sumActualReceivedMoney = response.data.sumActualReceivedMoney;
                    } else {
                        $scope.sumActualReceivedMoney = 0;
                    }
                    $scope.count = response.data.count;
                });
                apiService.get('DashboardPowerBI/', params, function (response) {
                    $scope.barData = [];
                    if (response.data.length > 0) {
                        for (var i = 0; i < response.data.length; i++) {
                            console.log(response.data[i].date);
                            $scope.barData.push({
                                y: $filter('date')(response.data[i].date, 'dd-MM-yyyy'),
                                a: response.data[i].sumActualReceivedMoney
                            });
                        }
                    } else {
                        var date = moment();
                        $scope.barData.push({
                            y: date,
                            a: 0
                        });
                    }
                });
            });
        }
    }
})();