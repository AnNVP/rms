(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboardPowerBI', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboardPowerBI', {
        url: '/dashboardPowerBI',
        templateUrl: 'app/pages/dashboardPowerBI/dashboardPowerBI.html',
        title: 'Dự tính COM',
        controller: 'dashboardPowerBICtrl',
        sidebarMeta: {
          icon: 'ion-folder',
          order: 5,
        },
      });
  }

})();