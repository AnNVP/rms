(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
    .controller('PageTopCtrl', PageTopCtrl);

  function PageTopCtrl($scope, $window, $cookies, $translate) {

    $scope.userImageUrl = $cookies.get('user_image_url');

    $scope.flags = {
      selected: $cookies.get('NG_TRANSLATE_LANG_KEY'),
      en: 'en',
      vi: 'vi',
      th: 'th',
    };

    $scope.changeLanguage = function(language) {
      $translate.use(language);
      $scope.flags.selected = language;
    };

    $scope.signout = function() {
      _.forEach($cookies.getAll(), function (value, key) {
        $cookies.remove(key);
      });
      $window.location.href = 'auth.html';
    };
  }

})();